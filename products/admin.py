from django.contrib import admin
from django.forms import TextInput, Textarea
from django.utils.html import format_html

from pigeon import settings
from suit.admin import SortableModelAdmin
from mptt.admin import MPTTModelAdmin
from .models import *
from tabbed_admin import TabbedModelAdmin
from easy_select2 import select2_modelform


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    max_num = 20
    extra = 1
    # formfield_overrides = {
    #     models.CharField: {'widget': TextInput(attrs={'size': '20'})},
    #     models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    # }


class ProductBetInline(admin.TabularInline):
    form = select2_modelform(ProductBet, attrs={'width': '250px'})
    model = ProductBet
    extra = 1


class ProductLanguageInline(admin.TabularInline):
    model = ProductLanguage
    max_num = len(settings.LANGUAGES)
    extra = len(settings.LANGUAGES)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }


class CategoryLanguageInline(admin.TabularInline):
    model = CategoryLanguage
    max_num = len(settings.LANGUAGES)
    extra = len(settings.LANGUAGES)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }


class AttributeLanguageInline(admin.TabularInline):
    model = AttributeLanguage
    max_num = len(LANGUAGES)
    extra = len(LANGUAGES)


class AttributeValueLanguageInline(admin.TabularInline):
    model = AttributeValueLanguage
    max_num = len(LANGUAGES)
    extra = len(LANGUAGES)


class AttributeOfProductInline(admin.TabularInline):
    # form = select2_modelform(AttributeOfProduct, attrs={'width': '250px'})
    model = AttributeOfProduct
    extra = 1


@admin.register(AttributeOfProduct)
class AttributeOfProductAdmin(admin.ModelAdmin):
    list_display = [field.name for field in AttributeOfProduct._meta.fields if field.name != "id"]


@admin.register(Product)
class ProductAdmin(TabbedModelAdmin):
    form = select2_modelform(Product, attrs={'width': '250px'})
    # change_form_template = 'admin_custom/product.html'
    list_display = ['status_admin', 'name_admin'] + [field.name for field in Product._meta.fields if field.name != "id" and field.name != "added_date" and field.name != "update_date"]
    list_editable = ('category_fk',)
    list_display_links = ['name_admin', 'status_admin']
    # inlines = (ProductLanguageInline,)
    tab_Overview = (
        (None, {
            'fields': (
                'first_price', 'min_price', 'image', 'category_fk', 'show_in_categories', 'active', 'start_date', 'end_date')
        }),
    )
    tab_Product_language = (
        ProductLanguageInline,
    )
    tab_Attribute = (
        AttributeOfProductInline,
    )
    tab_Additional_Image = (
        ProductImageInline,
    )
    tab_Product_Bet = (
        ProductBetInline,
    )
    tabs = [
        ('General', tab_Overview),
        ('ProductLanguage', tab_Product_language),
        ('Attribute', tab_Attribute),
        ('Additional Images', tab_Additional_Image),
        ('Bets', tab_Product_Bet)
    ]

    # inlines = (ProductLanguageInline,)

    class Meta:
        model = Product

    def name_admin(self, instance):
        return instance.__str__()

    def status_admin(self, instance):
        if instance.start_date < timezone.now() and timezone.now() > instance.end_date:
            print('end Time')
            return format_html(
                '<div style="color: red; font-size: 16px;">{}</div>',
                'Skończony',
            )
        elif instance.start_date < timezone.now():
            print('active')
            return format_html(
                '<div style="color: green; font-size: 16px;">{}</div>',
                'Aktywny',
            )
        elif instance.start_date > timezone.now():
            print('to bee')
            return format_html(
                '<div style="color: grey; font-size: 16px;">{}</div>',
                'Wkrótce',
            )

    status_admin.short_description = 'Status'


@admin.register(ProductBet)
class ProductBetAdmin(TabbedModelAdmin):
    form = select2_modelform(ProductBet, attrs={'width': '250px'})
    # change_form_template = 'admin_custom/product.html'
    list_display = [field.name for field in ProductBet._meta.fields if field.name != "id"]
    # inlines = (ProductLanguageInline,)
    tab_Overview = (
        (None, {
            'fields': (
                'price', 'product_fk', 'user_fk')
        }),
    )

    tabs = [
        ('General', tab_Overview),
    ]

    # inlines = (ProductLanguageInline,)

    class Meta:
        model = Product


@admin.register(Category)
class CategoryAdmin(MPTTModelAdmin, SortableModelAdmin):
    mptt_level_indent = 20
    list_display = ['name_admin'] + [field.name for field in Category._meta.fields]
    list_editable = ('parent',)
    list_display_links = ('id', 'name_admin',)
    sortable = 'order'
    inlines = (CategoryLanguageInline,)

    def name_admin(self, instance):
        return instance.__str__()


@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Attribute._meta.fields if field.name != "id"]
    inlines = (AttributeLanguageInline,)


@admin.register(AttributeValue)
class AttributeValueAdmin(admin.ModelAdmin):
    list_display = [field.name for field in AttributeValue._meta.fields if field.name != "id"]
    inlines = (AttributeValueLanguageInline,)


class AttributeOfProductAdmin(TabbedModelAdmin):
    list_display = [field.name for field in AttributeOfProduct._meta.fields if field.name != "id"]
