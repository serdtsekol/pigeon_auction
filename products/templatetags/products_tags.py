from django import template
from django.utils.translation import get_language

from ..get_data import get_all_categories, get_bet_winner_by_product_pk, get_last_bets
from ..models import Attribute, AttributeLanguage, Product, ProductLanguage

register = template.Library()

@register.simple_tag
def tag_get_last_lots(qty=8):
    last_products = Product.objects.filter(active=True).order_by('-start_date')[:qty]
    result = []
    for last_product in last_products:
        result.append(last_product.get_fields())
    return result

@register.simple_tag
def tag_get_all_attributes():
    return AttributeLanguage.objects.filter(language=get_language())

@register.filter
def filter_image_x2(value, x=2):
    """Фільтер для збільшеня width/height картинки в 2 рази"""
    return int(value) * x


@register.simple_tag
def tag_get_all_categories():
    return get_all_categories()


@register.simple_tag
def tag_get_bet_winner_by_product_pk(product_pk):
    return get_bet_winner_by_product_pk(product_pk)


@register.simple_tag
def tag_get_last_bets(count):
    return get_last_bets(count)
