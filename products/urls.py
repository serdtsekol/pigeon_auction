from django.contrib import admin
from django.urls import path, include, re_path

from .views import *
from .api import *

urlpatterns = [
    path('api/add_bet_json/', add_bet_json, name='add_bet_json'),
    path('catalog/', catalog, name="catalog"),
    path('archive/', archive, name="archive"),
    path('soon/', soon, name="soon"),
    path('search/', search, name="search"),
    path('create_pdf_product/<int:product_id>/', create_pdf_product, name='create_pdf_product'),
    re_path(r'^catalog/(?P<category_link>.+)/(?P<product_link>[\w\d\W]+)/$', product_or_category, name='product'),
    re_path(r'^catalog/(?P<category_link>.+)/$', category, name='category'),
    # re_path(r'^soon/(?P<category_link>.+)/(?P<product_link>[\w\d\W]+)/$', product_or_category, name='product'),
    re_path(r'^soon/(?P<category_link>.+)/$', category_soon, name='category_soon'),
    re_path(r'^api/get_attributes_json/$', get_attributes_json, name='get_attributes_list_in_category'),
    re_path(r'^api/get_filtered_products_json/$', get_filtered_products_json, name='get_filtered_products'),
    re_path(r'^api/get_attributes_search_json/$', get_attributes_search_json, name='get_attributes_search_json'),
    re_path(r'^api/get_filtered_products_search_json/$', get_filtered_products_search_json, name='get_filtered_products_search_json'),
    re_path(r'^api/get_attributes_archive_json/$', get_attributes_archive_json, name='get_attributes_archive_json'),
    re_path(r'^api/get_filtered_products_archive_json/$', get_filtered_products_archive_json, name='get_filtered_products_archive_json'),
    re_path(r'^api/get_attributes_soon_json/$', get_attributes_soon_json, name='get_attributes_soon_json'),
    re_path(r'^api/get_filtered_products_soon_json/$', get_filtered_products_soon_json, name='get_filtered_products_soon_json'),
    re_path(r'^api/get_all_products_json/$', get_all_products_json, name='get_all_products_json'),
    path('api/get_attribute_values_id_by_attribute_id_json/', get_attribute_values_id_by_attribute_id_json, name="get_attribute_values_id_by_attribute_id_json"),
    path('api/add_lot/', add_lot, name="add_lot"),
    path('api/get_attributes_values_by_attributes/', get_attributes_values_by_attributes, name="get_attributes_values_by_attributes"),
    path('api/send_file_to_server/', send_file_to_server, name="send_file_to_server")
]
