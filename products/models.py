from datetime import datetime, timedelta

import pytz
from django.contrib.auth.models import User
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import get_language

from ckeditor.fields import RichTextField
from django.core.exceptions import ValidationError
from django.db import models
from filebrowser.fields import FileBrowseField
from mainapp.models import Profile
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from django.urls import reverse

from pigeon.settings import LANGUAGES, TIME_ZONE
from django.template.defaultfilters import date
from django.template.defaulttags import now


class Category(MPTTModel):
    image = FileBrowseField("Image", max_length=200, directory="images/", extensions=[".jpg", ".webp", ".png"], blank=True)
    order = models.PositiveIntegerField(default=0)
    active = models.BooleanField(default=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    profile_fk = models.ForeignKey(Profile, on_delete=models.CASCADE)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    class MPTTMeta:
        order_insertion_by = ['order']

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        Category.objects.rebuild()

    def __str__(self):
        try:
            return str(self.categorylanguage_set.filter(language=get_language()).first().name)
        except Exception as ex:
            return str(self.id)

    def get_fields(self):
        category_lng = CategoryLanguage.objects.get(language=get_language(), category_fk=self.pk)
        result = {
            'name': category_lng.name,
            'title': category_lng.title,
            'description': category_lng.description,
            'image': self.profile_fk.image.url,
            # 'image': self.profile_fk.image,
            'link': self.get_absolute_url(),
            'language': category_lng.language,
            'added_date': self.added_date,
            'update_date': self.update_date,
        }
        return result

    def get_all_children_and_your_father(self):
        children = [self]
        try:
            child_list = self.children.all()
        except AttributeError:
            return children
        for child in child_list:
            children.extend(child.get_all_children_and_your_father())
        return children

    def get_all_children(self):
        all_children = self.get_all_children_and_your_father()
        all_children.remove(all_children[0])
        return all_children

    def get_all_parents(self):
        parents = [self]
        if self.parent is not None:
            parent = self.parent
            parents.extend(parent.get_all_parents())
        return parents

    def get_absolute_url(self):
        link = ""
        for parent in reversed(self.get_all_parents()):
            category_lng = CategoryLanguage.objects.get(language=get_language(), category_fk=parent.pk)
            link += category_lng.link + "/"
        link = link[:-1]
        return reverse('category', args=[str(link)])

    def get_absolute_url_soon(self):
        link = ""
        for parent in reversed(self.get_all_parents()):
            category_lng = CategoryLanguage.objects.get(language=get_language(), category_fk=parent.pk)
            link += category_lng.link + "/"
        link = link[:-1]
        return reverse('category_soon', args=[str(link)])

    def clean(self):
        if self.parent in self.get_all_children():
            raise ValidationError("Error Category")


class CategoryLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
    link = models.CharField(null=True, blank=True, default="", max_length=100)
    name = models.CharField(null=True, blank=True, default="", max_length=100)
    title = models.CharField(null=True, blank=True, default="", max_length=100)
    description = RichTextField(null=True, blank=True, default="")
    category_fk = models.ForeignKey(Category, on_delete=models.CASCADE)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.link:
            self.link = self.name.lower().replace(' ', '')
        if not self.title:
            self.title = self.name
        super(CategoryLanguage, self).save()


class Product(models.Model):
    min_price = models.IntegerField(default=0)
    first_price = models.IntegerField(default=0)
    image = FileBrowseField("Image", max_length=200, directory="product", extensions=[".jpg", ".webp", ".png"], blank=True)
    category_fk = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='main_category')
    show_in_categories = models.ManyToManyField(Category, blank=True)
    active = models.BooleanField(default=True)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    start_date = models.DateTimeField(auto_now_add=False, auto_now=False, default=datetime.now)
    end_date = models.DateTimeField(auto_now_add=False, auto_now=False, default=datetime.now)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     print(self.show_in_categories.all())
    #     print(self.show_in_categories)
    #     print(len(self.show_in_categories.all()))
    #     if len(self.show_in_categories.all()) <= 0:
    #         print("menhe")
    #         test = self.show_in_categories.all()
    #         self.show_in_categories.add(self.category_fk.id)
    #
    #         super(Product, self).save()
    #     super(Product, self).save()

    def __str__(self):
        try:
            return str(self.productlanguage_set.filter(language=get_language()).first().name)
        except Exception as ex:
            return str(self.id)

    def get_fields(self):
        product_lng = ProductLanguage.objects.get(language=get_language(), product_fk=self.pk)
        product_additional_images = ProductImage.objects.filter(product_fk=self.pk)
        product_bets = ProductBet.objects.filter(product_fk=self.pk).order_by('-price')

        current_price = self.first_price
        if product_bets.exists():
            for bet in product_bets:
                if current_price < bet.price:
                    current_price = bet.price

        time = None
        if self.start_date < timezone.now() and timezone.now() > self.end_date:
            time = _('Ставки закриті')
        elif self.start_date < timezone.now():
            time = _('Час до закінчення:') + '<br>' + str(self.end_date - timezone.now()).split('.')[0]
        elif self.start_date > timezone.now():
            time = _('Лот стартує о') + '<br><b>' + str(date(self.start_date.astimezone(pytz.timezone(TIME_ZONE)), 'Y-m-d H:i')) + "</b>"
        result = {
            'pk': self.pk,
            'name': product_lng.name,
            'first_price': self.first_price,
            'min_price': self.min_price,
            'current_price': current_price,
            'bets': product_bets,
            'title': product_lng.title,
            'description': product_lng.description,
            'image': self.image,
            'additional_images': product_additional_images,
            'link': self.get_absolute_url(),
            'language': product_lng.language,
            'category_fk': self.category_fk,
            'show_in_categories': self.show_in_categories,
            'added_date': self.added_date,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'to_end_date': time,
            'update_date': self.update_date,
        }
        return result

    def get_attributes(self):
        result = []
        # attributes = ProductLanguage.objects.get(language=get_language(), product_fk=self.pk)
        attrs_of_prod = AttributeOfProduct.objects.filter(product_fk=self.pk)
        for attr_of_prod in attrs_of_prod:
            try:
                attr = attr_of_prod.attribute_fk
                attr_lang = AttributeLanguage.objects.get(attribute_fk=attr.pk, language=get_language())
                attr_val = attr_of_prod.attribute_value_fk
                attr_val_lang = AttributeValueLanguage.objects.get(attribute_value_fk=attr_val.pk,
                                                                   language=get_language())

                result.append({
                    'attr_pk': attr.pk,
                    'product_pk': attr_of_prod.product_fk.pk,
                    'attr_value_pk': attr_val.pk,
                    'name': attr_lang.name,
                    'value': attr_val_lang.value,
                })
            except Exception as ex:
                print(ex)
        return result

    def get_absolute_url(self):
        product_lng = ProductLanguage.objects.get(language=get_language(), product_fk=self.pk)
        return str(self.category_fk.get_absolute_url() + str(product_lng.link) + "/")


def toppings_changed(sender, instance, action, **kwargs):
    print('action ', action)
    m2m_changed.disconnect(toppings_changed, sender=Product.show_in_categories.through)
    print(instance.show_in_categories)
    #if not instance.show_in_categories.filter(instance.category_fk.id).exists():
    # instance.show_in_categories.get_or_create(instance.category_fk.id)
    instance.save()

    m2m_changed.connect(toppings_changed, sender=Product.show_in_categories.through)

m2m_changed.connect(toppings_changed, sender=Product.show_in_categories.through)

@receiver(post_save, sender=Product)
def post_product_add_category(sender, instance, **kwargs):
    post_save.disconnect(post_product_add_category, sender=Product)
    print(instance.show_in_categories.all())
    instance.show_in_categories.clear()
    instance.show_in_categories.add(instance.category_fk.id)
    instance.save()
    post_save.connect(post_product_add_category, sender=Product)

    print('post save callback')

# @receiver([m2m_changed], sender=Product.show_in_categories.though)
# def post_m2m_changer_cart_item(sender, instance, *args, **kwargs):
#
#     post_save.disconnect(post_product_add_category, sender=Product.show_in_categories.though)
#     print(instance)
#     print("m2")
#     post_save.connect(post_product_add_category, sender=Product.show_in_categories.though)

class ProductLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
    link = models.CharField(null=True, blank=True, default="", max_length=100)
    name = models.CharField(null=True, blank=True, default="", max_length=100)
    title = models.CharField(null=True, blank=True, default="", max_length=100)
    description = RichTextField(null=True, blank=True, default="")
    product_fk = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.language)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.name:
            self.name = self.name
        if not self.link:
            self.link = self.name.lower().replace(' ', '')
        if not self.title:
            self.title = self.name
        super(ProductLanguage, self).save()


class ProductImage(models.Model):
    image = FileBrowseField("Image", max_length=200, directory="product", extensions=[".jpg", ".webp", ".png"], blank=True)
    product_fk = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.image)


class ProductBet(models.Model):
    price = models.IntegerField(default=0)
    user_fk = models.ForeignKey(User, on_delete=models.CASCADE)
    product_fk = models.ForeignKey(Product, on_delete=models.CASCADE)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        try:
            return self.product_fk.productlanguage_set.filter(language=get_language()).first().name + ": " + str(self.price)
        except:
            return str(self.id)


class Attribute(models.Model):
    name = models.CharField(null=False, max_length=100)

    def __str__(self):
        return str(self.name)


class AttributeLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
    name = models.CharField(null=False, max_length=100)
    attribute_fk = models.ForeignKey(Attribute, on_delete=models.CASCADE)


class AttributeValue(models.Model):
    name = models.CharField(null=False, max_length=100)
    attribute_fk = models.ForeignKey(Attribute, on_delete=models.CASCADE)

    # attribute_fk = models.ManyToManyField(Attribute)
    def __str__(self):
        return str(self.name)


class AttributeValueLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
    value = models.CharField(null=False, max_length=100)
    attribute_value_fk = models.ForeignKey(AttributeValue, on_delete=models.CASCADE)


class AttributeOfProduct(models.Model):
    attribute_fk = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    product_fk = models.ForeignKey(Product, on_delete=models.CASCADE)
    attribute_value_fk = models.ForeignKey(AttributeValue, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.attribute_fk.name)

    # def clean(self):
    #     if not self.attribute_fk.pk == self.attribute_value_fk.pk:
    #         raise ValidationError('The clients and the contacts company does' ' not match.')
