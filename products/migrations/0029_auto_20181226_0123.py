# Generated by Django 2.1.4 on 2018-12-25 23:23

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0028_auto_20181225_1559'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attributegrouplanguage',
            name='attribute_group_fk',
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='attribute_group_fk',
        ),
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 27, 1, 23, 3, 481217)),
        ),
        migrations.AlterField(
            model_name='product',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 26, 1, 23, 3, 481217)),
        ),
        migrations.DeleteModel(
            name='AttributeGroup',
        ),
        migrations.DeleteModel(
            name='AttributeGroupLanguage',
        ),
    ]
