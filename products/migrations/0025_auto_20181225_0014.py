# Generated by Django 2.1.4 on 2018-12-24 22:14

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0024_auto_20181225_0013'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 26, 0, 14, 10, 935489)),
        ),
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 26, 0, 14, 10, 935489)),
        ),
    ]
