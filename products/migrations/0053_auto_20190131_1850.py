# Generated by Django 2.1.4 on 2019-01-31 16:50

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0052_auto_20190120_1649'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 2, 1, 18, 50, 2, 820082)),
        ),
        migrations.AlterField(
            model_name='product',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 1, 31, 18, 50, 2, 820082)),
        ),
        migrations.AlterField(
            model_name='productlanguage',
            name='link',
            field=models.CharField(blank=True, default='', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='productlanguage',
            name='name',
            field=models.CharField(blank=True, default='', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='productlanguage',
            name='title',
            field=models.CharField(blank=True, default='', max_length=100, null=True),
        ),
    ]
