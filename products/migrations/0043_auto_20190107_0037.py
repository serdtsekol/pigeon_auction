# Generated by Django 2.1.4 on 2019-01-06 22:37

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0042_auto_20190106_0417'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 1, 8, 0, 37, 7, 151561)),
        ),
        migrations.AlterField(
            model_name='product',
            name='first_price',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='product',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2019, 1, 7, 0, 37, 7, 151561)),
        ),
        migrations.AlterField(
            model_name='productbet',
            name='price',
            field=models.IntegerField(default=0),
        ),
    ]
