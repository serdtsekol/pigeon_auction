# Generated by Django 2.1.4 on 2019-06-22 18:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0057_auto_20190502_0023'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='min_price',
            field=models.IntegerField(default=0),
        ),
    ]
