# Generated by Django 2.1.4 on 2018-12-25 23:38

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0029_auto_20181226_0123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 27, 1, 38, 46, 917828)),
        ),
        migrations.AlterField(
            model_name='product',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 26, 1, 38, 46, 917828)),
        ),
    ]
