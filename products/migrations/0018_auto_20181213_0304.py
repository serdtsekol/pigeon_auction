# Generated by Django 2.1.4 on 2018-12-13 01:04

import datetime
from django.db import migrations, models
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0017_auto_20181212_1529'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='image_v2',
            field=filebrowser.fields.FileBrowseField(blank=True, max_length=200, verbose_name='Image'),
        ),
        migrations.AlterField(
            model_name='product',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2018, 12, 14, 3, 4, 56, 150947)),
        ),
    ]
