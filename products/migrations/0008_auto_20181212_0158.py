# Generated by Django 2.1.4 on 2018-12-11 23:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0007_auto_20181212_0158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='image',
            field=models.ImageField(blank=True, default='no-image.webp', null=True, upload_to='category/'),
        ),
    ]
