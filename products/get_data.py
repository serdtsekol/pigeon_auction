import datetime

from django.db.models import Q
from django.template.defaultfilters import lower
from django.utils import timezone
from django.utils.timezone import now

from .models import Category, CategoryLanguage, Product, ProductLanguage, ProductBet, AttributeOfProduct, AttributeLanguage, AttributeValueLanguage, AttributeValue
from django.utils.translation import get_language

from orders.models import Order


def get_all_categories(is_parents=True, limit=1000):
    """ Віддає всі включенні категорії (is_parents=True Тільки батьків) """
    if is_parents:
        categories = Category.objects.filter(active=True, parent=None)[:limit]
    else:
        categories = Category.objects.filter(active=True)[:limit]
    result = []
    for category in categories:
        products_in_category_active = Product.objects.filter(category_fk_id=category.id, start_date__lte=now(), end_date__gte=now())
        if len(products_in_category_active) > 0:
            result.append({
                'object': category,
                'data': category.get_fields(),
            })
    return result


def get_all_soon_categories(is_parents=True, limit=1000):
    """ Віддає всі включенні категорії (is_parents=True Тільки батьків) """
    if is_parents:
        categories = Category.objects.filter(active=True, parent=None)[:limit]
    else:
        categories = Category.objects.filter(active=True)[:limit]
    result = []
    for category in categories:
        products_in_category_active = Product.objects.filter(category_fk_id=category.id, start_date__gte=now())
        if len(products_in_category_active) > 0:
            result.append({
                'object': category,
                'data': category.get_fields(),
            })
    return result


def get_all_category_children(pk):
    """ Віддає всіх дітей категорії """
    category = Category.objects.get(pk=pk, categorylanguage__language=get_language())
    result = []
    for category in category.get_all_children():
        result.append({
            'object': category,
            'data': category.get_fields(),
        })
    return result


def get_category_by_link(link):
    category = None
    try:
        category = Category.objects.get(categorylanguage__link=link, categorylanguage__language=get_language())
        result = {
            'object': category,
            'data': category.get_fields(),
            'redirect': False,
        }
    except Exception as ex:
        categories = Category.objects.filter(categorylanguage__link=link)
        for cat in categories:
            if cat.get_fields()['language'] == get_language():
                category = cat
        result = {
            'object': category,
            'data': category.get_fields(),
            'redirect': True,
        }
        print("Категорії потрібен редірект на іншу мову")
    return result


def get_all_archive_products():
    products = Product.objects.filter(active=True, end_date__lte=timezone.now())
    result = []
    for product in products:
        result.append({
            'object': product,
            'data': product.get_fields(),
        })
    return result


def get_all_soon_products():
    products = Product.objects.filter(active=True, start_date__gte=timezone.now())
    result = []
    for product in products:
        result.append({
            'object': product,
            'data': product.get_fields(),
        })
    return result


def get_all_products(request=None):
    """ Віддає всі включенні продукти """
    if request and request.GET.get('search_name'):
        search_name = request.GET.get('search_name')

        products = Product.objects.filter(
            Q(productlanguage__name__icontains=lower(search_name)) |
            Q(attributeofproduct__attribute_value_fk__name__icontains=lower(search_name)) |
            Q(category_fk__categorylanguage__name__icontains=lower(search_name)),
            active=True,
            start_date__lte=now(),
            end_date__gte=now()
        ).distinct()
    else:
        products = Product.objects.filter(active=True, start_date__lte=now(), end_date__gte=now())
    result = []
    for product in products:
        result.append({
            'object': product,
            'data': product.get_fields(),
        })
    return result


def get_all_products_in_category(category):
    """ Віддає всі продукти в категорії """
    products = Product.objects.filter(active=True, show_in_categories=category, start_date__lte=now(), end_date__gte=now()).order_by('-end_date')
    result = []
    for product in products:
        result.append({
            'object': product,
            'data': product.get_fields(),
        })
    return result


def get_all_products_in_category_soon(category):
    """ Віддає всі продукти в категорії """
    products = Product.objects.filter(active=True, show_in_categories=category, start_date__gte=now()).order_by('-start_date')
    result = []
    for product in products:
        result.append({
            'object': product,
            'data': product.get_fields(),
        })
    return result


def get_product_by_link(link):
    """ Віддає дані продукта по ссилці"""

    product = None
    try:
        product = Product.objects.get(active=True, productlanguage__link=link,
                                      productlanguage__language=get_language())
        result = {
            'object': product,
            'data': product.get_fields(),
            'redirect': False,
        }
    except Exception as ex:
        products = Product.objects.filter(active=True, productlanguage__link=link)
        for prod in products:
            if prod.get_fields()['language'] == get_language():
                product = prod
        result = {
            'object': product,
            'data': product.get_fields(),
            'redirect': True,
        }

    return result


def get_product_by_pk(pk):
    """ Віддає дані продукта по його id """
    product = Product.objects.get(active=True, pk=pk, productlanguage__language=get_language())
    result = {
        'object': product,
        'data': product.get_fields(),
    }
    return result


def get_last_bets(count=100):
    result = {
        'object': ProductBet.objects.all().order_by('-added_date')[:count],
        'data': {}
    }
    return result


def get_bets_by_user_pk(user_pk):
    """ Віддає ставки по юзер id """
    products_bets = ProductBet.objects.filter(user_fk_id=user_pk).order_by('-added_date')
    result = []
    for product_bet in products_bets:
        result.append({
            'object': product_bet,
            'data': {'product': product_bet.product_fk.get_fields()}
        })
    return result


def get_bets_win_by_user_pk(user_pk):
    """ Віддає виграні ставки по юзер id """

    products_bets = []
    products = ProductBet.objects.values('product_fk').filter(user_fk_id=user_pk, product_fk__end_date__lte=now()).order_by('product_fk')

    products_pks = []
    [products_pks.append(pk['product_fk']) for pk in products if pk['product_fk'] not in products_pks]
    for product_pk in products_pks:
        bet = ProductBet.objects.filter(product_fk_id=product_pk, product_fk__end_date__lte=now()).order_by('-added_date').first()
        if bet.user_fk.pk == user_pk:
            products_bets.append(bet)

    result = []
    for product_bet in products_bets:
        order = Order.objects.filter(product_id=product_bet.product_fk.pk, user_id=user_pk).first()  # Замовлення

        result.append({
            'object': product_bet,
            'data': {'product': product_bet.product_fk.get_fields(), 'payment': True if order else False}
        })
    return result


def get_bet_winner_by_product_pk(product_pk):
    """ Получає виграшну ставку по в продукті """
    bet = ProductBet.objects.filter(product_fk_id=product_pk, product_fk__end_date__lte=now()).order_by('-added_date').first()
    result = {
        'object': bet,
        'data': {}
    }
    return result


def get_attributes_list(_products=None, _attributes=None):
    attributes = list()
    result = []
    products = []
    if _products:  # Якщо у функцію приходить список продуктів
        if _attributes:  # Якщо у функцію приходить хоча б один обраний параметр
            attr_val_temp = []  # Створюємо темп масив
            for attr_val in _attributes:
                attr_val_temp.extend(attr_val['values'])  # Додаємо кожен value зі всіх атрибутів в темп масив

            attributes_of_product_temp = AttributeOfProduct.objects.filter(  # Запит на отримання об'єктів AttributeOfProduct які попадають у вибірку
                product_fk__in=[product['pk'] for product in _products],
                attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                product_fk__active=True,
                product_fk__start_date__lte=now(),
                product_fk__end_date__gte=now()
            )
            products = []

            for product in _products:
                compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
                compare_attr_values_from_table = sorted(
                    [att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if
                     att_prd.product_fk.pk == product['pk']])
                if compare_attr_values == compare_attr_values_from_table:
                    products.append(Product.objects.get(pk=product['pk']))
        else:  # Жоден атрибут не обрано
            products = Product.objects.filter(pk__in=[product['pk'] for product in _products], active=True, start_date__lte=now(), end_date__gte=now())
    else:
        products = Product.objects.filter(active=True, start_date__lte=now(), end_date__gte=now())

    attr_of_prods = AttributeOfProduct.objects.filter(product_fk__in=[product.pk for product in products], product_fk__active=True, product_fk__start_date__lte=now(),
                                                      product_fk__end_date__gte=now())
    for product in products:
        attributes_of_product = product.get_attributes()
        for attribute in attributes_of_product:
            if attribute['attr_pk'] not in attributes:
                attributes.append(attribute['attr_pk'])
    for attribute in attributes:
        att_lang = AttributeLanguage.objects.get(attribute_fk=attribute, language=get_language())
        attr_values_filtered = list()

        for attr_val in attr_of_prods:
            if attribute == attr_val.attribute_fk.pk:
                if attr_val.attribute_value_fk.pk not in attr_values_filtered:
                    attr_values_filtered.append(attr_val.attribute_value_fk.pk)

        result_attr_values = list()
        for attr_val in attr_values_filtered:
            attr_val_lang = AttributeValueLanguage.objects.get(attribute_value_fk=attr_val, language=get_language())
            result_attr_values.append(
                {'pk': attr_val, 'attr_pk': attribute, 'name': attr_val_lang.value,
                 'count_products': attr_of_prods.filter(attribute_fk=attribute, attribute_value_fk=attr_val, product_fk__start_date__lte=now(), product_fk__end_date__gte=timezone.now()).count()})
        result.append({
            'attr': {'pk': attribute, 'name': att_lang.name},
            'attr_values': result_attr_values
        })
    return result


def get_all_products_and_filter_attributes(_attributes):
    products = []
    if _attributes:
        attr_val_temp = []
        for attr_val in _attributes:
            attr_val_temp.extend(attr_val['values'])
        attributes_of_product_temp = AttributeOfProduct.objects.filter(product_fk__active=True, attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                                                                       attribute_value_fk__in=[val['attr_value_pk'] for val in attr_val_temp], product_fk__start_date__lte=now(),
                                                                       product_fk__end_date__gte=now())

        _products_pk = []
        [_products_pk.append(att_of_prod.product_fk.pk) for att_of_prod in attributes_of_product_temp if att_of_prod.product_fk.pk not in _products_pk]
        for _product_pk in _products_pk:
            compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
            compare_attr_values_from_table = sorted([att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if att_prd.product_fk.pk == _product_pk])
            if compare_attr_values == compare_attr_values_from_table:
                products.append(Product.objects.get(pk=_product_pk))
    else:
        products = Product.objects.filter(active=True, start_date__lte=now(), end_date__gte=now()).order_by('-end_date')

    result = []

    for product in products:
        fields_product = product.get_fields()
        result.append(
            {'pk': fields_product['pk'], 'name': fields_product['name'], 'link': fields_product['link'], 'image': fields_product['image'].url, 'current_price': fields_product['current_price'],
             'time': fields_product['to_end_date']})
    return result


def get_attributes_archive_list(_products=None, _attributes=None):
    attributes = list()
    result = []
    products = []
    if _products:  # Якщо у функцію приходить список продуктів
        if _attributes:  # Якщо у функцію приходить хоча б один обраний параметр
            attr_val_temp = []  # Створюємо темп масив
            for attr_val in _attributes:
                attr_val_temp.extend(attr_val['values'])  # Додаємо кожен value зі всіх атрибутів в темп масив

            attributes_of_product_temp = AttributeOfProduct.objects.filter(  # Запит на отримання об'єктів AttributeOfProduct які попадають у вибірку
                product_fk__in=[product['pk'] for product in _products],
                attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                product_fk__active=True,
                product_fk__end_date__lte=timezone.now()
            )
            products = []

            for product in _products:
                compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
                compare_attr_values_from_table = sorted(
                    [att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if
                     att_prd.product_fk.pk == product['pk']])
                if compare_attr_values == compare_attr_values_from_table:
                    products.append(Product.objects.get(pk=product['pk']))
        else:  # Жоден атрибут не обрано
            products = Product.objects.filter(pk__in=[product['pk'] for product in _products], active=True, end_date__lte=timezone.now())
    else:
        products = Product.objects.filter(active=True)

    attr_of_prods = AttributeOfProduct.objects.filter(product_fk__in=[product.pk for product in products], product_fk__active=True, product_fk__end_date__lte=timezone.now())
    for product in products:
        attributes_of_product = product.get_attributes()
        for attribute in attributes_of_product:
            if attribute['attr_pk'] not in attributes:
                attributes.append(attribute['attr_pk'])
    for attribute in attributes:
        att_lang = AttributeLanguage.objects.get(attribute_fk=attribute, language=get_language())
        attr_values_filtered = list()

        for attr_val in attr_of_prods:
            if attribute == attr_val.attribute_fk.pk:
                if attr_val.attribute_value_fk.pk not in attr_values_filtered:
                    attr_values_filtered.append(attr_val.attribute_value_fk.pk)

        result_attr_values = list()
        for attr_val in attr_values_filtered:
            attr_val_lang = AttributeValueLanguage.objects.get(attribute_value_fk=attr_val, language=get_language())
            result_attr_values.append(
                {'pk': attr_val, 'attr_pk': attribute, 'name': attr_val_lang.value,
                 'count_products': attr_of_prods.filter(attribute_fk=attribute, attribute_value_fk=attr_val, product_fk__end_date__lte=timezone.now()).count()})
        result.append({
            'attr': {'pk': attribute, 'name': att_lang.name},
            'attr_values': result_attr_values
        })
    return result


def get_all_products_and_filter_archive_attributes(_attributes):
    products = []
    if _attributes:
        attr_val_temp = []
        for attr_val in _attributes:
            attr_val_temp.extend(attr_val['values'])
        attributes_of_product_temp = AttributeOfProduct.objects.filter(product_fk__active=True, attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                                                                       attribute_value_fk__in=[val['attr_value_pk'] for val in attr_val_temp], product_fk__end_date__lte=timezone.now())

        _products_pk = []
        [_products_pk.append(att_of_prod.product_fk.pk) for att_of_prod in attributes_of_product_temp if att_of_prod.product_fk.pk not in _products_pk]
        for _product_pk in _products_pk:
            compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
            compare_attr_values_from_table = sorted([att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if att_prd.product_fk.pk == _product_pk])
            if compare_attr_values == compare_attr_values_from_table:
                products.append(Product.objects.get(pk=_product_pk))
    else:
        products = Product.objects.filter(active=True, end_date__lte=timezone.now()).order_by('-end_date')

    result = []

    for product in products:
        fields_product = product.get_fields()
        result.append(
            {'pk': fields_product['pk'], 'name': fields_product['name'], 'link': fields_product['link'], 'image': fields_product['image'].url, 'current_price': fields_product['current_price'],
             'time': fields_product['to_end_date']})
    return result


def get_attributes_soon_list(category_pk=None, _products=None, _attributes=None):
    attributes = list()
    result = []
    products = []
    if _products:  # Якщо у функцію приходить список продуктів
        if _attributes:  # Якщо у функцію приходить хоча б один обраний параметр
            attr_val_temp = []  # Створюємо темп масив
            for attr_val in _attributes:
                attr_val_temp.extend(attr_val['values'])  # Додаємо кожен value зі всіх атрибутів в темп масив

            attributes_of_product_temp = AttributeOfProduct.objects.filter(  # Запит на отримання об'єктів AttributeOfProduct які попадають у вибірку
                product_fk__in=[product['pk'] for product in _products],
                attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                product_fk__active=True,
                product_fk__start_date__gte=timezone.now(),
                product_fk__show_in_categories=category_pk,
            )
            products = []

            for product in _products:
                compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
                compare_attr_values_from_table = sorted(
                    [att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if
                     att_prd.product_fk.pk == product['pk']])
                if compare_attr_values == compare_attr_values_from_table:
                    products.append(Product.objects.get(pk=product['pk']))
        else:  # Жоден атрибут не обрано
            products = Product.objects.filter(pk__in=[product['pk'] for product in _products],show_in_categories=category_pk, active=True, start_date__gte=now())
    else:
        products = Product.objects.filter(active=True, show_in_categories=category_pk, start_date__gte=now())

    attr_of_prods = AttributeOfProduct.objects.filter(product_fk__in=[product.pk for product in products], product_fk__active=True, product_fk__start_date__gte=timezone.now(), product_fk__show_in_categories=category_pk)
    for product in products:
        attributes_of_product = product.get_attributes()
        for attribute in attributes_of_product:
            if attribute['attr_pk'] not in attributes:
                attributes.append(attribute['attr_pk'])
    for attribute in attributes:
        att_lang = AttributeLanguage.objects.get(attribute_fk=attribute, language=get_language())
        attr_values_filtered = list()

        for attr_val in attr_of_prods:
            if attribute == attr_val.attribute_fk.pk:
                if attr_val.attribute_value_fk.pk not in attr_values_filtered:
                    attr_values_filtered.append(attr_val.attribute_value_fk.pk)

        result_attr_values = list()
        for attr_val in attr_values_filtered:
            attr_val_lang = AttributeValueLanguage.objects.get(attribute_value_fk=attr_val, language=get_language())
            result_attr_values.append(
                {'pk': attr_val, 'attr_pk': attribute, 'name': attr_val_lang.value,
                 'count_products': attr_of_prods.filter(attribute_fk=attribute, attribute_value_fk=attr_val, product_fk__start_date__gte=timezone.now()).count()})
        result.append({
            'attr': {'pk': attribute, 'name': att_lang.name},
            'attr_values': result_attr_values
        })
    return result


def get_all_products_and_filter_soon_attributes(category_pk, _attributes):
    products = []
    if _attributes:
        attr_val_temp = []
        for attr_val in _attributes:
            attr_val_temp.extend(attr_val['values'])
        attributes_of_product_temp = AttributeOfProduct.objects.filter(product_fk__active=True, attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                                                                       attribute_value_fk__in=[val['attr_value_pk'] for val in attr_val_temp], product_fk__start_date__gte=timezone.now(), product_fk__category_fk_id=category_pk)

        _products_pk = []
        [_products_pk.append(att_of_prod.product_fk.pk) for att_of_prod in attributes_of_product_temp if att_of_prod.product_fk.pk not in _products_pk]
        for _product_pk in _products_pk:
            compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
            compare_attr_values_from_table = sorted([att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if att_prd.product_fk.pk == _product_pk])
            if compare_attr_values == compare_attr_values_from_table:
                products.append(Product.objects.get(pk=_product_pk))
    else:
        products = Product.objects.filter(active=True, start_date__gte=timezone.now(), category_fk_id=category_pk).order_by('-end_date')

    result = []

    for product in products:
        fields_product = product.get_fields()
        result.append(
            {'pk': fields_product['pk'], 'name': fields_product['name'], 'link': fields_product['link'], 'image': fields_product['image'].url, 'current_price': fields_product['current_price'],
             'time': fields_product['to_end_date']})
    return result


def get_attributes_list_in_category(category_pk, _products=None, _attributes=None):
    attributes = list()
    result = []
    products = []
    if _products:  # Якщо у функцію приходить список продуктів
        if _attributes:  # Якщо у функцію приходить хоча б один обраний параметр
            attr_val_temp = []  # Створюємо темп масив
            for attr_val in _attributes:
                attr_val_temp.extend(attr_val['values'])  # Додаємо кожен value зі всіх атрибутів в темп масив

            attributes_of_product_temp = AttributeOfProduct.objects.filter(  # Запит на отримання об'єктів AttributeOfProduct які попадають у вибірку
                product_fk__in=[product['pk'] for product in _products],
                attribute_fk__in=[attr['attr_pk'] for attr in _attributes], product_fk__start_date__lte=now(), product_fk__end_date__gte=now())
            products = []

            for product in _products:
                compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
                compare_attr_values_from_table = sorted(
                    [att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if
                     att_prd.product_fk.pk == product['pk']])
                if compare_attr_values == compare_attr_values_from_table:
                    products.append(Product.objects.get(pk=product['pk']))
        else:  # Жоден атрибут не обрано
            products = Product.objects.filter(pk__in=[product['pk'] for product in _products], start_date__lte=now(), end_date__gte=now())
    else:
        products = Product.objects.filter(show_in_categories=category_pk, start_date__lte=now(), end_date__gte=now())

    attr_of_prods = AttributeOfProduct.objects.filter(product_fk__in=[product.pk for product in products], product_fk__start_date__lte=now(), product_fk__end_date__gte=now())
    for product in products:
        attributes_of_product = product.get_attributes()
        for attribute in attributes_of_product:
            if attribute['attr_pk'] not in attributes:
                attributes.append(attribute['attr_pk'])
    for attribute in attributes:
        att_lang = AttributeLanguage.objects.get(attribute_fk=attribute, language=get_language())
        attr_values_filtered = list()

        for attr_val in attr_of_prods:
            if attribute == attr_val.attribute_fk.pk:
                if attr_val.attribute_value_fk.pk not in attr_values_filtered:
                    attr_values_filtered.append(attr_val.attribute_value_fk.pk)

        result_attr_values = list()
        for attr_val in attr_values_filtered:
            attr_val_lang = AttributeValueLanguage.objects.get(attribute_value_fk=attr_val, language=get_language())
            result_attr_values.append(
                {'pk': attr_val, 'attr_pk': attribute, 'name': attr_val_lang.value,
                 'count_products': attr_of_prods.filter(attribute_fk=attribute, attribute_value_fk=attr_val, product_fk__start_date__lte=now(), product_fk__end_date__gte=timezone.now()).count()})
        result.append({
            'attr': {'pk': attribute, 'name': att_lang.name},
            'attr_values': result_attr_values
        })
    return result


def get_all_products_in_category_and_filter_attributes(category_pk, _attributes):
    products = []
    if _attributes:
        attr_val_temp = []
        for attr_val in _attributes:
            attr_val_temp.extend(attr_val['values'])
        attributes_of_product_temp = AttributeOfProduct.objects.filter(product_fk__active=True, product_fk__show_in_categories=category_pk, attribute_fk__in=[attr['attr_pk'] for attr in _attributes],
                                                                       attribute_value_fk__in=[val['attr_value_pk'] for val in attr_val_temp], product_fk__start_date__lte=now(),
                                                                       product_fk__end_date__gte=timezone.now())

        _products_pk = []
        [_products_pk.append(att_of_prod.product_fk.pk) for att_of_prod in attributes_of_product_temp if att_of_prod.product_fk.pk not in _products_pk]
        for _product_pk in _products_pk:
            compare_attr_values = sorted([val['attr_value_pk'] for val in attr_val_temp])
            compare_attr_values_from_table = sorted([att_prd.attribute_value_fk.pk for att_prd in attributes_of_product_temp if att_prd.product_fk.pk == _product_pk])
            if compare_attr_values == compare_attr_values_from_table:
                products.append(Product.objects.get(pk=_product_pk))
    else:
        products = Product.objects.filter(active=True, show_in_categories=category_pk, start_date__lte=now(), end_date__gte=timezone.now()).order_by('-end_date')

    result = []

    for product in products:
        fields_product = product.get_fields()
        result.append(
            {'pk': fields_product['pk'], 'name': fields_product['name'], 'link': fields_product['link'], 'image': fields_product['image'].url, 'current_price': fields_product['current_price'],
             'time': fields_product['to_end_date']})
    return result
