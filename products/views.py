import os
from io import BytesIO
from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings
from django.template.loader import render_to_string, get_template

from .models import *
from django.utils.translation import get_language
from .get_data import *
import xhtml2pdf.pisa as pisa


# Create your views here.

def catalog(request):
    result_request = {
        "categories": get_all_categories(),
    }
    return render(request, 'products/catalog.html', result_request)


def product_or_category(request, category_link, product_link):
    print("PRODUCT_CATEGOPY")
    category_slugs = [str(x) for x in category_link.split('/')]
    try:
        product = get_product_by_link(product_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if product['redirect']:
            return redirect(product['object'].get_absolute_url())

        result_request = {
            "product": product,
            "step_bet": settings.STEP_BET,
        }
        print("is product: " + product['object'].__str__())
        return render(request, 'products/product.html', result_request)
    except Exception as ex:
        print('not product: ',ex)
    try:
        category_slugs.append(product_link)  # Додаю до категорій останню категорію тому що це не продукт
        category = get_category_by_link(product_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if category['redirect']:
            return redirect(category['object'].get_absolute_url())

        result_request = {
            "category": category,
            "categories_children": get_all_category_children(category['object'].pk),
            "products": get_all_products_in_category(category['object'].pk)
        }
        print("is category: " + category['object'].__str__())
        return render(request, 'products/category.html', result_request)
    except Exception as ex:
        print(ex)
        print("not category")

    raise Http404("NotProductAndNOtCategory")


def category(request, category_link):
    print("CATEGOPY")
    try:
        category = get_category_by_link(category_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if category['redirect']:
            return redirect(category['object'].get_absolute_url())

        category_parents = category['object'].get_all_parents()

        if len(category_parents) > 1:
            raise Http404("IsCategoryButUrlBad")
        result_request = {
            "category": category,
            "categories_children": get_all_category_children(category['object'].pk),
            "products": get_all_products_in_category(category['object'].pk)
        }
        return render(request, 'products/category.html', result_request)
    except Exception as ex:
        print(ex)
    raise Http404("NotCategory")


def archive(request):
    result_request = {
        "products": get_all_archive_products()
    }
    return render(request, 'products/catalog_archive.html', result_request)


def soon(request):
    result_request = {
        "categories": get_all_soon_categories(),
    }
    # result_request = {
    #     "products": get_all_soon_products()
    # }
    return render(request, 'products/catalog_soon.html', result_request)


def category_soon(request, category_link):
    print("CATEGOPY SOON")
    try:
        category = get_category_by_link(category_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """

        if category['redirect']:
            return redirect(category['object'].get_absolute_url_soon())

        category_parents = category['object'].get_all_parents()

        if len(category_parents) > 1:
            raise Http404("IsCategoryButUrlBad")
        result_request = {
            "category": category,
            "categories_children": get_all_category_children(category['object'].pk),
            "products": get_all_products_in_category_soon(category['object'].pk)
        }
        return render(request, 'products/category_soon.html', result_request)
    except Exception as ex:
        print(ex)
    raise Http404("NotCategory")


def search(request):
    result_request = {
        "products": get_all_products(request)
    }
    return render(request, 'products/products_search.html', result_request)


def fetch_pdf_resources(uri, rel):
    if uri.find(settings.MEDIA_URL) != -1:
        path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ''))
    elif uri.find(settings.STATIC_URL) != -1:
        path = os.path.join(settings.STATIC_ROOT, uri.replace(settings.STATIC_URL, ''))
    else:
        path = None
    return path


def create_pdf_product(request, product_id):
    product = get_product_by_pk(product_id)
    template = get_template('products/product_pdf.html')
    html = template.render({'product': product, 'main_url': request.scheme + "://" + request.META['HTTP_HOST']})
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode('UTF-8')), response,
                            encoding='utf-8',
                            link_callback=fetch_pdf_resources)
    # pdf = pisa.pisaDocument(BytesIO(render_to_string('products/product_pdf.html', {'product': product}, request).encode("UTF-8")), response)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse("Error Rendering PDF", status=400)
