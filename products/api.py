import base64
import decimal
import errno
import json
import os
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect, render
from django.utils.translation import get_language
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from slugify import slugify

from filebrowser.base import FileObject
from filebrowser.settings import ADMIN_THUMBNAIL
from pigeon.settings import LANGUAGES, MEDIA_ROOT
from .models import ProductBet, Product, AttributeValue, ProductLanguage, Category, CategoryLanguage, AttributeValueLanguage, AttributeOfProduct, ProductImage
from .get_data import get_attributes_list_in_category, get_all_products_in_category_and_filter_attributes, get_all_products_and_filter_attributes, get_attributes_list, get_attributes_archive_list, \
    get_all_products_and_filter_archive_attributes, get_attributes_soon_list, get_all_products_and_filter_soon_attributes
import json


def save_file(file_byte, name):
    if not os.path.exists(os.path.dirname(name)):
        try:
            os.makedirs(os.path.dirname(name))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    try:
        with open(name, "wb") as file:
            file.write(file_byte)
        FileObject(name).version_generate(ADMIN_THUMBNAIL)
    except Exception as ex:
        print(ex)


@csrf_exempt
def send_file_to_server(request):
    print(request.body)
    print(request.FILES['file'])
    return HttpResponse('ok')


@csrf_exempt
def get_attributes_values_by_attributes(request):
    id_attribute = request.POST.get('id', None)
    attribute_values = AttributeValueLanguage.objects.filter(language=get_language(), attribute_value_fk__attribute_fk_id=id_attribute)
    return render(request, 'mainapp/options.html', {'options': attribute_values})


@csrf_exempt
def add_lot(request):
    data = json.loads(request.POST.get('data'))
    attributes_of_values = data.get('attributes_of_values', [])
    array_images = data.get('array_images', [])

    first_price = data.get('first_price', 0)
    time_start = data.get('time_start', None)
    time_end = data.get('time_end', None)
    languages = data.get('languages', None)

    for lang in languages:
        if languages[lang]['name'] is None or languages[lang]['name'] == "":
            return JsonResponse({'success': False, 'error': _('Add Title In All Languages')}, safe=False)

    if first_price is None or first_price == "":
        return JsonResponse({'success': False, 'error': _('Select First Price')}, safe=False)
    if array_images is None or array_images == "" or len(array_images) == 0:
        return JsonResponse({'success': False, 'error': _('Select Image')}, safe=False)
    if time_start is None or time_start == "":
        return JsonResponse({'success': False, 'error': _('Select Time Start')}, safe=False)
    if time_end is None or time_end == "":
        return JsonResponse({'success': False, 'error': _('Select Time End')}, safe=False)

    time_start = datetime.strptime(time_start, '%Y-%m-%dT%H:%M')
    time_end = datetime.strptime(time_end, '%Y-%m-%dT%H:%M')

    category, category_created_bool = Category.objects.update_or_create(profile_fk_id=request.user.profile.id, defaults={'active': True})
    for lang, descr_lang in LANGUAGES:
        CategoryLanguage.objects.update_or_create(
            language=lang, category_fk_id=category.id,
            defaults={
                'link': slugify(f'{request.user.first_name}_{request.user.last_name}{category.id}', to_lower=True),
                'name': f'{request.user.first_name} {request.user.last_name}',
                'title': f'{request.user.first_name} {request.user.last_name}',
                'description': 'TestDescription'
            }
        )
    for image in array_images:
        imgdata = base64.b64decode(image['image_content'].split('base64,')[-1])
        save_file(imgdata, f"{MEDIA_ROOT}/product/{slugify(f'{request.user.first_name}_{request.user.last_name}', to_lower=True)}/{image['name']}")
    product = Product.objects.create(first_price=first_price, min_price=first_price, start_date=time_start, end_date=time_end, image=FileObject(f"product/{slugify(f'{request.user.first_name}_{request.user.last_name}', to_lower=True)}/{array_images[0]['name']}"), active=False, category_fk_id=category.id)
    product.show_in_categories.add(category.id)

    product.save()
    if len(array_images) > 1:
        coun_i = 0
        for image in array_images:
            if coun_i == 0:
                coun_i += 1
                continue
            ProductImage.objects.create(product_fk_id=product.id, image=FileObject(f"product/{slugify(f'{request.user.first_name}_{request.user.last_name}', to_lower=True)}/{image['name']}"))

    for att_of_val in attributes_of_values:
        AttributeOfProduct.objects.update_or_create(product_fk_id=product.id, attribute_fk_id=att_of_val['attribute_fk_id'], attribute_value_fk_id=att_of_val['attribute_value_fk_id'])

    for lang, descr_lang in LANGUAGES:
        name = languages[lang]['name']
        description = languages[lang]['description']
        link = slugify(name, to_lower=True)
        ProductLanguage.objects.create(language=lang, name=name, title=name, description=description, link=link, product_fk_id=product.id)

    data_r = {
        'success': True,
    }

    return JsonResponse(data_r, safe=False)


def add_bet_json(request):
    data = request.POST
    price = int(data.get('price', 0))
    user_id = request.user.pk
    product_id = data.get('product-pk', 0)

    max_bet_price = 0
    product_bets = ProductBet.objects.filter(product_fk_id=product_id).order_by('price')

    for product_bet in product_bets:
        if product_bet.price > max_bet_price:
            max_bet_price = product_bet.price

    if product_bets.exists():
        if product_bets.last().user_fk.pk == user_id:
            result_request = {'success': False, 'message': 'Ви вже зробили ставку'}
            return JsonResponse(result_request, safe=False)

    if max_bet_price >= price:
        result_request = {'success': False, 'message': 'Ціна меньша ніж попередня ставка'}
        return JsonResponse(result_request, safe=False)

    try:
        product_bet_save = ProductBet.objects.create(price=price, user_fk_id=user_id, product_fk_id=product_id)
        product_bet_save.save()
        result_request = {'success': True, 'message': 'Ставка зробленна успішно'}
        return JsonResponse(result_request, safe=False)
    except Exception as ex:
        print(ex)
        result_request = {'success': False, 'message': 'Невдалось зробити ставку'}
        return JsonResponse(result_request, safe=False)
    # user = User.objects.get(pk=user_id)


def get_attributes_json(request):
    category_pk = request.POST.get('category_pk')
    products = json.loads(request.POST.get('products'))
    attributes = json.loads(request.POST.get('attributes'))
    attr_list = get_attributes_list_in_category(category_pk, products, attributes)
    return JsonResponse(attr_list, safe=False)


def get_filtered_products_json(request):
    attributes = json.loads(request.POST.get('attributes'))
    category_pk = request.POST.get('category')
    result = {
        'products': get_all_products_in_category_and_filter_attributes(category_pk, attributes),
    }
    return JsonResponse(result, safe=False)


def get_attributes_search_json(request):
    products = json.loads(request.POST.get('products'))
    attributes = json.loads(request.POST.get('attributes'))
    attr_list = get_attributes_list(products, attributes)
    return JsonResponse(attr_list, safe=False)


def get_filtered_products_search_json(request):
    attributes = json.loads(request.POST.get('attributes'))
    result = {
        'products': get_all_products_and_filter_attributes(attributes),
    }
    return JsonResponse(result, safe=False)


def get_attributes_archive_json(request):
    products = json.loads(request.POST.get('products'))
    attributes = json.loads(request.POST.get('attributes'))
    attr_list = get_attributes_archive_list(products, attributes)
    return JsonResponse(attr_list, safe=False)


def get_filtered_products_archive_json(request):
    attributes = json.loads(request.POST.get('attributes'))
    result = {
        'products': get_all_products_and_filter_archive_attributes(attributes),
    }
    return JsonResponse(result, safe=False)


def get_attributes_soon_json(request):
    category_pk = request.POST.get('category_pk')
    products = json.loads(request.POST.get('products'))
    attributes = json.loads(request.POST.get('attributes'))
    attr_list = get_attributes_soon_list(category_pk, products, attributes)
    return JsonResponse(attr_list, safe=False)


def get_filtered_products_soon_json(request):
    attributes = json.loads(request.POST.get('attributes'))
    category_pk = request.POST.get('category')
    result = {
        'products': get_all_products_and_filter_soon_attributes(category_pk, attributes),
    }
    return JsonResponse(result, safe=False)


def get_all_products_json(request):
    products = []
    for product in Product.objects.all():
        products.append({'pk': product.pk, 'name': product.productlanguage_set.filter(language=get_language()).first().name, 'price': product.price})

    response = JsonResponse(products, safe=False)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Methods'] = 'GET, POST'
    response['Access-Control-Allow-Headers'] = '*'
    return response


def get_attribute_values_id_by_attribute_id_json(request):
    attribute_id = request.GET.get("attribute_id")
    result = []
    if attribute_id:
        attributes_values = AttributeValue.objects.filter(attribute_fk=attribute_id)
        for attribute_value in attributes_values:
            result.append({'id': attribute_value.pk, 'name': attribute_value.name})
    return JsonResponse(result, safe=False)
