��          �   %   �      0     1  6   L     �  9   �  )   �  ;     +   C  "   o  2   �  '   �     �  
   �            @     :   `  6   �  F   �  L     E   f  F   �  C   �  &   7  �  ^  "   �  4         ;  S   \  ;   �  Y   �  8   F	  #   	  :   �	  %   �	     
     
     +
     ;
  G   L
  B   �
  >   �
  X     ^   o  V   �  Y   %  W     /   �           	                                     
                                                                               %s tag requires a queryset %s tag requires either three, seven or eight arguments %s tag requires three arguments A node may not be made a child of any of its descendants. A node may not be made a child of itself. A node may not be made a sibling of any of its descendants. A node may not be made a sibling of itself. An invalid position was given: %s. Cannot insert a node which has already been saved. Delete selected %(verbose_name_plural)s First child Last child Left sibling Right sibling drilldown_tree_for_node tag was given an invalid model field: %s drilldown_tree_for_node tag was given an invalid model: %s full_tree_for_model tag was given an invalid model: %s if eight arguments are given, fifth argument to %s tag must be 'count' if eight arguments are given, fourth argument to %s tag must be 'cumulative' if eight arguments are given, seventh argument to %s tag must be 'in' if seven arguments are given, fourth argument to %s tag must be 'with' if seven arguments are given, sixth argument to %s tag must be 'in' second argument to %s tag must be 'as' Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: BARTOSZ BIAŁY <ethifus@gmain.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 tag %s wymaga argumentu 'queryset' tag %s wymaga trzech, siedmiu lub ośmiu argumentów tag %s wymaga trzech argumentów Węzeł nie może zostać węzłem potomnym żadnego ze swoich węzłów potomnych. Węzeł nie może zostać swoim własnym węzłem potomnym. Węzeł nie może zostać węzłem równożędnym żadnego ze swoich węzłów potomnych. Węzeł nie może zostać swoim węzłem równożędnym. Podano niewłaściwą pozycję: %s. Nie można wstawić węzła, który został już zapisany. Usuń wybrane %(verbose_name_plural)s Pierwszy podelement Ostatni podelement Lewy podelement Prawy podelement do tagu drilldown_tree_for_node przekazano niewłaściwe pole model: %s do tagu drilldown_tree_for_node przekazano niewłaściwy model: %s do tagu full_tree_for_model przekazano niewłaściwy model: %s jeżeli podano osiem argumentów, to piątym argumentem tagu %s musi być słowo 'count' jeżeli podano osiem argumentów, to czwartym argumentem tagu %s musi być słowo 'cumulative' jeżeli podano osiem argumentów, to siódmym argumentem tagu %s musi być słowo 'in' jeżeli podano siedem argumentów, to czwartym argumentem tagu %s musi być słowo 'with' jeżeli podano siedem argumentów, to szóstym argumentem tagu %s musi być słowo 'in' drugim argumentem tagu %s musi być słowo 'as' 