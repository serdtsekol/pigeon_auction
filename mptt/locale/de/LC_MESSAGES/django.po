# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: django-mptt\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-13 18:41+0300\n"
"PO-Revision-Date: 2016-08-24 12:32+0200\n"
"Last-Translator: Henning Hraban Ramm <hraban@fiee.net>\n"
"Language-Team: fiëé visuëlle <hraban@fiee.net>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: .\mptt\admin.py:88
#, python-format
msgid "Successfully deleted %(count)d items."
msgstr "%(count)d Einträge gelöscht."

#: .\mptt\admin.py:101
#, python-format
msgid "Delete selected %(verbose_name_plural)s"
msgstr "Ausgewählte %(verbose_name_plural)s löschen"

#: .\mptt\admin.py:144
msgid "title"
msgstr "Titel"

#: .\mptt\admin.py:178
msgid "Did not understand moving instruction."
msgstr "Unbekannter Verschiebe-Befehl."

#: .\mptt\admin.py:186
msgid "Objects have disappeared, try again."
msgstr "Objekte sind verschwunden, bitte noch einmal versuchen."

#: .\mptt\admin.py:190
msgid "No permission"
msgstr "Keine Berechtigung"

#: .\mptt\admin.py:199
#, python-format
msgid "Database error: %s"
msgstr "Datenbankfehler: %s"

#: .\mptt\admin.py:204
#, python-format
msgid "%s has been successfully moved."
msgstr "%s wurde erfolgreich verschoben."

#: .\mptt\admin.py:215
msgid "move node before node"
msgstr "Knoten vor den Knoten verschieben"

#: .\mptt\admin.py:216
msgid "move node to child position"
msgstr "Knoten als Unterknoten einfügen"

#: .\mptt\admin.py:217
msgid "move node after node"
msgstr "Knoten nach den Knoten verschieben"

#: .\mptt\admin.py:218
msgid "Collapse tree"
msgstr "Alles zuklappen"

#: .\mptt\admin.py:219
msgid "Expand tree"
msgstr "Alles aufklappen"

#: .\mptt\admin.py:327
msgid "All"
msgstr "Alle"

#: .\mptt\apps.py:9
msgid "mptt"
msgstr ""

#: .\mptt\forms.py:63
msgid "First child"
msgstr "Erstes Unterelement"

#: .\mptt\forms.py:64
msgid "Last child"
msgstr "Letztes Unterelement"

#: .\mptt\forms.py:65
msgid "Left sibling"
msgstr "Linker Nachbar"

#: .\mptt\forms.py:66
msgid "Right sibling"
msgstr "Rechter Nachbar"

#: .\mptt\forms.py:184
msgid "Invalid parent"
msgstr "Ungültiges Eltern-Element"

#: .\mptt\managers.py:522
msgid "Cannot insert a node which has already been saved."
msgstr "Kann ein Element, welches schon gespeichert wurde, nicht einfügen."

#: .\mptt\managers.py:739 .\mptt\managers.py:897 .\mptt\managers.py:933
#: .\mptt\managers.py:1097
#, python-format
msgid "An invalid position was given: %s."
msgstr "Eine ungültige Position wurde angegeben: %s."

#: .\mptt\managers.py:883 .\mptt\managers.py:1077
msgid "A node may not be made a sibling of itself."
msgstr ""
"Ein Element kann nicht in ein Geschwister von sich selbst umgewandelt werden."

#: .\mptt\managers.py:1056 .\mptt\managers.py:1174
msgid "A node may not be made a child of itself."
msgstr "Ein Element kann nicht Unterelement von sich selbst sein."

#: .\mptt\managers.py:1058 .\mptt\managers.py:1176
msgid "A node may not be made a child of any of its descendants."
msgstr "Ein Element kann nicht Unterelement eines seiner Unterlemente sein."

#: .\mptt\managers.py:1079
msgid "A node may not be made a sibling of any of its descendants."
msgstr ""
"Ein Element kann nicht ein Geschwister eines seiner Unterelemente sein."

#: .\mptt\models.py:301
msgid "register() expects a Django model class argument"
msgstr "register() erwartet als Argument eine Django-Modell-Klasse"

#: .\mptt\templates\admin\mptt_filter.html:3
#, python-format
msgid " By %(filter_title)s "
msgstr " Nach %(filter_title)s "

#: .\mptt\templatetags\mptt_tags.py:31
#, python-format
msgid "full_tree_for_model tag was given an invalid model: %s"
msgstr "full_tree_for_model Tag bekam ein ungültiges Modell: %s"

#: .\mptt\templatetags\mptt_tags.py:55
#, python-format
msgid "drilldown_tree_for_node tag was given an invalid model: %s"
msgstr "drilldown_tree_for_node-Tag bekam ein ungültiges Modell: %s"

#: .\mptt\templatetags\mptt_tags.py:62
#, python-format
msgid "drilldown_tree_for_node tag was given an invalid model field: %s"
msgstr "drilldown_tree_for_node-Tag bekam ein ungültiges Modellfeld: %s"

#: .\mptt\templatetags\mptt_tags.py:89
#, python-format
msgid "%s tag requires three arguments"
msgstr "%s Tag benötigt drei Argumente"

#: .\mptt\templatetags\mptt_tags.py:91 .\mptt\templatetags\mptt_tags.py:146
#, python-format
msgid "second argument to %s tag must be 'as'"
msgstr "Zweites Argument für %s-Tag muss 'as' sein"

#: .\mptt\templatetags\mptt_tags.py:143
#, python-format
msgid "%s tag requires either three, seven or eight arguments"
msgstr "%s-Tag benötigt entweder drei, sieben oder acht Argumente"

#: .\mptt\templatetags\mptt_tags.py:150
#, python-format
msgid "if seven arguments are given, fourth argument to %s tag must be 'with'"
msgstr ""
"wenn '%s' sieben Argumente übergeben werden, muss das vierte 'with' sein"

#: .\mptt\templatetags\mptt_tags.py:154
#, python-format
msgid "if seven arguments are given, sixth argument to %s tag must be 'in'"
msgstr ""
"wenn '%s' sieben Argumente übergeben werden, muss das sechste 'in' sein"

#: .\mptt\templatetags\mptt_tags.py:160
#, python-format
msgid ""
"if eight arguments are given, fourth argument to %s tag must be 'cumulative'"
msgstr ""
"wenn '%s' acht Argumente übergeben werden, muss das vierte 'cumulative' sein"

#: .\mptt\templatetags\mptt_tags.py:164
#, python-format
msgid "if eight arguments are given, fifth argument to %s tag must be 'count'"
msgstr ""
"wenn '%s' acht Argumente übergeben werden, muss das fünfte 'count' sein"

#: .\mptt\templatetags\mptt_tags.py:168
#, python-format
msgid "if eight arguments are given, seventh argument to %s tag must be 'in'"
msgstr "wenn '%s' acht Argumente übergeben werden, muss das achte 'in' sein"

#: .\mptt\templatetags\mptt_tags.py:287
#, python-format
msgid "%s tag requires a queryset"
msgstr "%s-Tag benötigt ein Queryset"

#: .\mptt\utils.py:251
#, python-format
msgid "Node %s not in depth-first order"
msgstr "Knoten %s ist nicht in Tiefe-zuerst-Reihenfolge"

#~ msgid "The model %s has already been registered."
#~ msgstr "%s wurde schon registriert."
