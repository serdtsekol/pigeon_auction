# Generated by Django 2.0.7 on 2018-11-23 08:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('send_email', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedbackform',
            name='question_field',
            field=models.BooleanField(default=True),
        ),
    ]
