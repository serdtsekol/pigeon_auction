from django import template
from django.template import context
from django.template.loader import render_to_string

register = template.Library()


@register.simple_tag
def tag_get_feedback():
    return render_to_string('send_email/first_form.html')
