from django.http import HttpResponse, JsonResponse
import django
from django.conf import settings
from django.core.mail import send_mail
import json
from .models import Feedback
from django.utils.translation import ugettext_lazy as _
from .models import *
from django.core import serializers


def success(request):
    error = []

    name = request.POST.get('name', '')
    email = request.POST.get('email', '')
    phone = request.POST.get('phone', '')
    comment = request.POST.get('comment', '')

    data = ""
    if name != "":
        data += _('name') + ": " + name + "\n"
    if email != "":
        data += _('email') + ": " + email + "\n"
    if phone != "":
        data += _('phone') + ": " + phone + "\n"
    if comment != "":
        data += _('comment') + ": " + comment + "\n"

    if not name:
        error.append({'name': _('name_empty')})
    if not email:
        error.append({'email': _('email_empty')})
    if not phone:
        error.append({'phone': _('phone_empty')})
    if not comment:
        error.append({'comment': _('comment_empty')})

    if len(error) > 0:
        return JsonResponse({'success': False, 'error': error}, safe=False)
    else:
        try:
            send_mail('ipa24.com ContactForm', data, settings.DEFAULT_FROM_EMAIL, settings.DEFAULT_TO_EMAIL, fail_silently=False)
            return JsonResponse({'success': True, 'error': None}, safe=False)
        except Exception as ex:
            error.append({'send': error})
            return JsonResponse({'success': False, 'error': error}, safe=False)
