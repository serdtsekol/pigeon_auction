import os

from django.db import models


# Create your models here.

class Feedback(models.Model):
    name = models.CharField(max_length=100, blank=True, default=None)
    email = models.CharField(max_length=100, blank=True, default=None)
    phone = models.CharField(max_length=100, blank=True, default=None)
    comment = models.TextField(blank=True, default=None)

    def __str__(self):
        return self.name
