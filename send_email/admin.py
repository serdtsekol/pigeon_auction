from django.contrib import admin
from .models import Feedback


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Feedback._meta.fields]
    list_editable = [field.name for field in Feedback._meta.fields if field.name != "id"]

    class Meta:
        model = Feedback
