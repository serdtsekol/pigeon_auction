from django.contrib import admin
from django.urls import path, include, re_path
from .views import *
from paypal.standard.ipn.views import ipn

urlpatterns = [
    path('paypal/', test_ipn, name='ipn-test'),
    path('process/', payment_process, name='process'),
    path('done/', payment_done, name='done'),
    path('canceled/', payment_canceled, name='canceled'),
]
