from django.shortcuts import render, get_object_or_404
from django.conf import settings
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from django.views.decorators.csrf import csrf_exempt
from orders.models import Order


@csrf_exempt
def test_ipn(request):
    print(request.POST.get('test_ipn'))
    print(request.POST.get('payer_status'))
    print(request.POST.get('payment_status'))
    print(request.POST.get('verify_sign'))
    print(request.POST.get('txn_id'))
    print(request.POST.get('receiver_id'))
    print(request.POST.get('ipn_track_id'))
    print(request.POST.get('mc_currency'))
    print(request.POST.get('item_name'))
    if request.POST.get('payment_status') == 'Completed':
        order = get_object_or_404(Order, pk=request.POST.get('item_name'))
        order.status = True
        order.save()
    else:
        order = get_object_or_404(Order, pk=request.POST.get('item_name'))
        order.status = False
        order.save()
    return render(request, 'payment/done.html')


@csrf_exempt
def payment_done(request):
    # TODO треба добавити замовлення в ордери
    return render(request, 'payment/done.html')


@csrf_exempt
def payment_canceled(request):
    return render(request, 'payment/canceled.html')


def payment_process(request):
    # TODO треба зробити модель замовлень
    order_id = request.session.get('order_id')
    order = get_object_or_404(Order, pk=order_id)
    host = request.get_host()
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': order.price_PLN,
        'item_name': '{}'.format(order.pk),
        'currency_code': 'PLN',
        'notify_url': 'http://{}{}'.format(host, reverse('ipn-test')),
        'return_url': 'http://{}{}'.format(host, reverse('done')),
        'cancel_return': 'http://{}{}'.format(host, reverse('canceled')),
    }
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'payment/process.html', {'order': order.pk, 'form': form})
