��    V      �     |      x  &   y     �  &   �  #   �                      a   $     �     �     �     �     �     �     �     �     �     �     �     �  ^   	     c	  >   j	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     -
     2
     7
     =
     L
     Z
  
   _
  C   j
     �
     �
  $   �
  )   �
          5     =     J     Z     i     n     �     �     �     �     �     �     �  '   �       R   -  S   �  L   �  "   !  $   D  
   i  	   t  	   ~     �     �     �     �     �     �     �  
   �     �     �     �     �  <   �  6   &  �  ]  *   �        +   @  1   l     �     �     �     �  x   �     9  
   ?     J  	   S     ]     e     z     �     �  "   �  	   �      �  P   �     1  L   8     �     �     �     �  	   �     �     �     �                '     @     F     K     P     ^  	   p     z  Q   �     �     �  #     :   )     d  	   �     �     �     �     �     �     �  
   �     �       
   $     /     N  -   k     �  [   �  g     e   {     �                 -  	   9     C     I     M     Q     X  	   _     i     o     |     �  	   �  	   �  H   �  I   �             T   *   M   6      '          +   <             >   V      
           4   #   C   9          %   /   E              K                           B   ?   O   5       I   F   H   3   A       N   &   1              P   0   S                  -      7                    D          8             Q       (   =                           :   !            L           ,       .         @      $       )       G               "   2       U          ;          R   J   	        %(counter)s result %(counter)s results %(full_result_count)s total ... and %(escaped_object)s more Files. Action applied successfully to '%s' Actions All Allowed Any Date Are you sure you want to delete "%(escaped_object)s"? All of the following items will be deleted: Audio By Date By Type Cancel Change Confirm delete Date Delete Document Drop files here to upload Edit Error creating folder. Error finding Upload-Folder (site.storage.location + site.directory). Maybe it does not exist? Error. Extension %(ext)s is not allowed. Only %(allowed)s is allowed. Failed File Information File not found FileBrowser Filename Filesize Filter Flip horizontal Flip vertical Folder Folder Information Help Home Image Image Versions Max. Filesize Name New Folder Only letters, numbers, underscores, spaces and hyphens are allowed. Past 7 days Permission denied. Please correct the following errors. Please enable Javascript to upload files. Renaming was successful. Results Rotate 180° Rotate 90° CCW Rotate 90° CW Save Save and continue editing Search Select Select files to upload Size Submit Successfully deleted %s The File already exists. The Folder %s was successfully created. The Folder already exists. The Name will be converted to lowercase. Spaces will be replaced with underscores. The Name will be normalized by converting or stripping all non-standard characters. The files are being uploaded, if you leave now the upload will be cancelled. The requested File does not exist. The requested Folder does not exist. This Month This year Thumbnail Today Type URL Upload Upload a file Versions Video View Image Yes, I'm sure result results total {file} has invalid extension. Only {extensions} are allowed. {file} is too large, maximum file size is {sizeLimit}. Project-Id-Version: django-filebrowser
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2013-12-06 08:13+0000
Last-Translator: Patrick Kranzlmueller <patrick@vonautomatisch.at>
Language-Team: German (http://www.transifex.com/projects/p/django-filebrowser/language/de/)
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %(counter)s Resultat %(counter)s Resultate %(full_result_count)s insgesamt ... und %(escaped_object)s weitere Dateien. Die Aktion wurde erfolgreich auf '%s' angewendet. Aktionen Alle Erlaubt jedes Datum Sind Sie sicher, dass Sie "%(escaped_object)s" löschen wollen? Alle nachfolgend aufgelisteten Dateien werden gelöscht: Audio nach Datum nach Art Abbrechen Ändern Löschen bestätigen Datum Löschen Dokument Dateien für den Upload auswählen Editieren Fehler beim anlegen des Ordners. Der Upload-Folder (site.storage.location + site.directory) wurde nicht gefunden. Fehler Die Dateierweiterung %(ext)s ist nicht erlaubt. Nur %(allowed)s ist erlaubt. Fehler beim Upload. Informationen zur Datei Die Datei wurde nicht gefunden FileBrowser Dateiname Dateigröße Filter Horizontal spiegeln Vertikal spiegeln Ordner Informationen zum Ordner Hilfe Home Bild Bildversionen Max. Dateigröße Dateiname Neuer Ordner Nur Buchstaben, Nummern, Unterstriche, Leerzeichen und Bindestriche sind erlaubt. Letzte Woche Fehlende Zugriffsberechtigung. Bitte die folgenden Fehler beheben. Sie müssen Javascript aktivieren, um Uploads vorzunehmen. Das Umbenennen war erfolgreich. Resultate 180° drehen 90° gegen den UZS drehen 90° im UZS drehen Sichern Sichern und weiter editieren Suche Auswählen Dateien zum Upload auswählen Größe Abschicken %s wurde erfolgreich gelöscht Die Datei existiert bereits. Der Ordner %s wurde erfolgreich hinzugefügt. Der Ordner existiert bereits. Der Name wird in Kleinbuchstaben umgewandelt. Leerzeichen werden mit Unterstrichen ersetzt. Nicht alphanumerischen Zeichen – außer Unterstriche, Abstände und Bindestriche – werden entfernt. Die Dateien werden gerade upgeloadet. Wenn Sie die Seite jetzt verlassen wird der Upload abgebrochen. Die Datei ist nicht vorhanden. Der Ordner ist nicht vorhanden. Dieser Monat Dieses Jahr Thumbnail Heute Art URL Upload Upload Versionen Video Bild ansehen Ja, ich bin sicher Resultat Resultate insgesamt {file} hat eine ungültige Dateiendung. Nur {extensions} sind zulässig. {file} ist zu groß. Die maximal zulässige Dateigröße ist {sizeLimit}. 