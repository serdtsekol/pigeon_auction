import hashlib
from datetime import datetime, timedelta
from django.utils.translation import get_language

from ckeditor.fields import RichTextField
from django.core.exceptions import ValidationError
from django.db import models
from filebrowser.fields import FileBrowseField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from django.urls import reverse

from pigeon.settings import LANGUAGES


class Category(MPTTModel):
    name = models.CharField(null=False, max_length=100)
    image = FileBrowseField("Image", max_length=200, directory="blog/", extensions=[".jpg", ".webp", ".png"], blank=True)
    order = models.PositiveIntegerField(default=0)
    active = models.BooleanField(default=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    class MPTTMeta:
        order_insertion_by = ['order']

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        Category.objects.rebuild()

    def __unicode__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    def get_fields(self):
        category_lng = CategoryLanguage.objects.get(language=get_language(), category_fk=self.pk)
        result = {
            'pk': self.pk,
            'name': category_lng.name,
            'title': category_lng.name,
            'description': category_lng.description,
            'image': self.image,
            'link': self.get_absolute_url(),
            'language': category_lng.language,
            'added_date': self.added_date,
            'update_date': self.update_date,
        }
        return result

    def get_all_children_and_your_father(self):
        children = [self]
        try:
            child_list = self.children.all()
        except AttributeError:
            return children
        for child in child_list:
            children.extend(child.get_all_children_and_your_father())
        return children

    def get_all_children(self):
        all_children = self.get_all_children_and_your_father()
        all_children.remove(all_children[0])
        return all_children

    def get_all_parents(self):
        parents = [self]
        if self.parent is not None:
            parent = self.parent
            parents.extend(parent.get_all_parents())
        return parents

    def get_absolute_url(self):
        link = ""
        for parent in reversed(self.get_all_parents()):
            category_lng = CategoryLanguage.objects.get(language=get_language(), category_fk=parent.pk)
            link += category_lng.link + "/"
        link = link[:-1]
        return reverse('category_blog', args=[str(link)])

    def clean(self):
        if self.parent in self.get_all_children():
            raise ValidationError("Error Category")


class CategoryLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
    link = models.CharField(null=False, max_length=100)
    name = models.CharField(null=False, max_length=100)
    title = models.CharField(null=False, max_length=100)
    description = RichTextField(null=True, blank=True, default="")
    category_fk = models.ForeignKey(Category, on_delete=models.CASCADE)


class Article(models.Model):
    name = models.CharField(null=False, max_length=100)
    image = FileBrowseField("Image", max_length=200, directory="blog/", extensions=[".jpg", ".webp", ".png"], null=False)
    category_fk = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='main_category')
    show_in_categories = models.ManyToManyField(Category, related_name='show_in_categories')
    show_in_index = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.name)

    def get_fields(self):
        article_lng = ArticleLanguage.objects.get(language=get_language(), article_fk=self.pk)
        result = {
            'name': article_lng.name,
            'title': article_lng.title,
            'short_description': article_lng.short_description,
            'description': article_lng.description,
            'image': self.image.url,
            'link': self.get_absolute_url(),
            'language': article_lng.language,
            'category_fk': self.category_fk,
            'show_in_categories': self.show_in_categories,
            'show_in_index': self.show_in_index,
            'added_date': self.added_date,
            'update_date': self.update_date,
        }
        return result

    def get_absolute_url(self):
        article_lng = ArticleLanguage.objects.get(language=get_language(), article_fk=self.pk)
        return str(self.category_fk.get_absolute_url() + str(article_lng.link) + "/")


class ArticleLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
    link = models.CharField(null=False, max_length=100)
    name = models.CharField(null=False, max_length=100)
    title = models.CharField(null=False, max_length=100)
    short_description = models.TextField(null=True, blank=True, default="")
    description = RichTextField(null=True, blank=True, default="")
    article_fk = models.ForeignKey(Article, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.language)
