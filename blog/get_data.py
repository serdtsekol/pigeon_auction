from .models import Category, CategoryLanguage, Article, ArticleLanguage
from django.utils.translation import get_language


def get_all_categories(is_parents=True):
    """ Віддає всі включенні категорії (is_parents=True Тільки батьків) """
    if is_parents:
        categories = Category.objects.filter(active=True, parent=None)
    else:
        categories = Category.objects.filter(active=True)
    result = []
    for category in categories:
        result.append({
            'object': category,
            'data': category.get_fields(),
        })
    return result


def get_all_category_children(pk):
    """ Віддає всіх дітей категорії """
    category = Category.objects.get(pk=pk, categorylanguage__language=get_language())
    result = []
    for category in category.get_all_children():
        result.append({
            'object': category,
            'data': category.get_fields(),
        })
    return result


def get_category_by_link(link):
    category = None
    try:
        category = Category.objects.get(categorylanguage__link=link, categorylanguage__language=get_language())
        result = {
            'object': category,
            'data': category.get_fields(),
            'redirect': False,
        }
    except Exception as ex:
        categories = Category.objects.filter(categorylanguage__link=link)
        for cat in categories:
            if cat.get_fields()['language'] == get_language():
                category = cat
        result = {
            'object': category,
            'data': category.get_fields(),
            'redirect': True,
        }
        print("Категорії потрібен редірект на іншу мову")
    return result


def get_category_by_pk(pk):
    category = None
    category = Category.objects.get(pk=pk)
    result = {
        'object': category,
        'data': category.get_fields(),
        'redirect': False,
    }
    return result


def get_last_articles(limit=100):
    """ Віддає всі включенні останні статті """
    articles = Article.objects.filter(active=True).order_by('-added_date')[:limit]
    result = []
    for article in articles:
        result.append({
            'object': article,
            'data': article.get_fields(),
        })
    return result


def get_all_articles():
    """ Віддає всі включенні статті """
    articles = Article.objects.filter(active=True).order_by('-added_date')
    result = []
    for article in articles:
        result.append({
            'object': article,
            'data': article.get_fields(),
        })
    return result


def get_all_articles_in_category(category):
    """ Віддає всі статті в категорії """
    articles = Article.objects.filter(active=True, show_in_categories=category).order_by('-added_date')
    result = []
    for article in articles:
        result.append({
            'object': article,
            'data': article.get_fields(),
        })
    return result


def get_article_by_link(link):
    """ Віддає дані статті по ссилці"""
    article = None
    try:
        article = Article.objects.get(active=True, articlelanguage__link=link, articlelanguage__language=get_language())
        result = {
            'object': article,
            'data': article.get_fields(),
            'redirect': False,
        }
    except Exception as ex:
        articles = Article.objects.filter(active=True, articlelanguage__link=link)
        for prod in articles:
            if prod.get_fields()['language'] == get_language():
                article = prod
        result = {
            'object': article,
            'data': article.get_fields(),
            'redirect': True,
        }
    return result


def get_article_by_pk(pk):
    """ Віддає дані статті по його id """
    article = Article.objects.get(active=True, pk=pk, articlelanguage__language=get_language())
    result = {
        'object': article,
        'data': article.get_fields(),
    }
    return result
