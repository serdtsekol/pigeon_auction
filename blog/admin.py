from django.contrib import admin
from django.forms import TextInput, Textarea

from pigeon import settings
from suit.admin import SortableModelAdmin
from mptt.admin import MPTTModelAdmin
from tabbed_admin import TabbedModelAdmin
from .admin_actions import *


class ArticleLanguageInline(admin.TabularInline):
    model = ArticleLanguage
    max_num = len(settings.LANGUAGES)
    extra = len(settings.LANGUAGES)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }


class CategoryLanguageInline(admin.TabularInline):
    model = CategoryLanguage
    max_num = len(settings.LANGUAGES)
    extra = len(settings.LANGUAGES)
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }


@admin.register(Article)
class ArticleAdmin(TabbedModelAdmin):
    actions = [action_copy_article]
    # change_form_template = 'admin_custom/article.html'
    list_display = [field.name for field in Article._meta.fields if field.name != "id"]
    list_editable = ('active',)
    search_fields = ('name',)
    # inlines = (ArticleLanguageInline,)
    tab_Overview = (
        (None, {
            'fields': (
                'name', 'image', 'category_fk', 'show_in_categories', 'active', 'show_in_index')
        }),
    )
    tab_Article_language = (
        ArticleLanguageInline,
    )

    tabs = [
        ('General', tab_Overview),
        ('ArticleLanguage', tab_Article_language),
    ]

    # inlines = (ArticleLanguageInline,)

    class Meta:
        model = Article


@admin.register(Category)
class CategoryAdmin(MPTTModelAdmin, SortableModelAdmin):
    mptt_level_indent = 20
    list_display = [field.name for field in Category._meta.fields]
    list_editable = ('parent',)
    list_display_links = ('id', 'name',)
    sortable = 'order'
    inlines = (CategoryLanguageInline,)
