from .models import *


def action_copy_article(modeladmin, request, queryset):
    for article in queryset:
        _tempArticle = Article.objects.get(pk=article.pk)

        save_article = Article.objects.create(name=_tempArticle.name, category_fk=_tempArticle.category_fk, image=_tempArticle.image, active=False)
        [save_article.show_in_categories.add(cat) for cat in _tempArticle.show_in_categories.all()]
        save_article.save()

        for lang in LANGUAGES:
            _tempArticleLanguage = ArticleLanguage.objects.get(article_fk=_tempArticle, language=lang[0])
            save_article_language = ArticleLanguage.objects.create(name=_tempArticleLanguage.name,
                                                                   title=_tempArticleLanguage.title,
                                                                   article_fk_id=save_article.pk,
                                                                   language=lang[0],
                                                                   description=_tempArticleLanguage.description,
                                                                   short_description=_tempArticleLanguage.short_description,
                                                                   link=_tempArticleLanguage.link)
            save_article_language.save()
