# Generated by Django 2.1.4 on 2019-01-13 12:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20190112_1929'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articlelanguage',
            name='language',
            field=models.CharField(choices=[('pl', 'Polski'), ('en', 'English'), ('de', 'Germany')], max_length=15, verbose_name='lang'),
        ),
        migrations.AlterField(
            model_name='categorylanguage',
            name='language',
            field=models.CharField(choices=[('pl', 'Polski'), ('en', 'English'), ('de', 'Germany')], max_length=15, verbose_name='lang'),
        ),
    ]
