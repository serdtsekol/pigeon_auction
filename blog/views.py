from django.core.paginator import Paginator
from django.http import Http404
from django.shortcuts import render, redirect
from .models import *
from .get_data import *

BLOG_ITEMS_PER_PAGE = 10


# Create your views here.
def blog_sorting(request):
    articles = Article.objects.values().filter(show_in_index=True).order_by('name')
    for article in articles:
        article['link'] = Article.objects.get(id=article['id']).get_absolute_url()

    import itertools
    from operator import itemgetter
    # sorted_animals = sorted(animals, key=itemgetter('size'))
    new_articles = []
    for key, group in itertools.groupby(articles, key=lambda x: x['name'][0]):
        new_articles.append({'key': key, 'articles': list(group)})

    return render(request, 'blog/blog_sorting.html', {'articles': new_articles, "categories": get_all_categories(),})


def blog(request):
    articles = get_all_articles()
    paginator = Paginator(articles, BLOG_ITEMS_PER_PAGE)
    if request.method == "GET":
        page = paginator.page(request.GET.get('page', 1))
        articles = page
    result_request = {
        "categories": get_all_categories(),
        "articles": articles,
    }

    return render(request, 'blog/blog.html', result_request)


def article_or_category(request, category_link, article_link):
    print("ARTICLE_CATEGOPY")
    category_slugs = [str(x) for x in category_link.split('/')]
    try:
        article = get_article_by_link(article_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if article['redirect']:
            return redirect(article['object'].get_absolute_url())

        result_request = {
            "article": article,
            "category": get_category_by_pk(article['object'].category_fk.pk),
            "categories": get_all_categories(),
        }
        print("is article: " + article['object'].name)
        return render(request, 'blog/article.html', result_request)
    except Exception as ex:
        print(ex)
        print("not article")

    try:
        category_slugs.append(article_link)  # Додаю до категорій останню категорію тому що це не продукт
        category = get_category_by_link(article_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if category['redirect']:
            return redirect(category['object'].get_absolute_url())

        articles = get_all_articles_in_category(category['object'].pk)
        paginator = Paginator(articles, BLOG_ITEMS_PER_PAGE)
        if request.method == "GET":
            page = paginator.page(request.GET.get('page', 1))
            articles = page
        result_request = {
            "category": category,
            "categories": get_all_categories(),
            "categories_children": get_all_category_children(category['object'].pk),
            "articles": articles,
        }
        print("is category: " + category['object'].name)
        return render(request, 'blog/category.html', result_request)
    except Exception as ex:
        print(ex)
        print("not category")

    raise Http404("NotProductAndNOtCategory")


def category(request, category_link):
    print("CATEGOPY")
    try:
        category = get_category_by_link(category_link)

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if category['redirect']:
            return redirect(category['object'].get_absolute_url())

        category_parents = category['object'].get_all_parents()

        if len(category_parents) > 1:
            raise Http404("IsCategoryButUrlBad")

        articles = get_all_articles_in_category(category['object'].pk)
        paginator = Paginator(articles, BLOG_ITEMS_PER_PAGE)
        if request.method == "GET":
            page = paginator.page(request.GET.get('page', 1))
            articles = page

        result_request = {
            "category": category,
            "categories": get_all_categories(),
            "categories_children": get_all_category_children(category['object'].pk),
            "articles": articles,
        }
        return render(request, 'blog/category.html', result_request)
    except Exception as ex:
        print(ex)
    raise Http404("NotCategory")
