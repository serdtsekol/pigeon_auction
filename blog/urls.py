from django.contrib import admin
from django.urls import path, include, re_path

from .views import *


urlpatterns = [
    path('blog/', blog, name="blog"),
    path('blog_sorting/', blog_sorting, name="blog_sorting"),
    re_path(r'^blog/(?P<category_link>.+)/(?P<article_link>[\w\d\W]+)/$', article_or_category, name='article'),
    re_path(r'^blog/(?P<category_link>.+)/$', category, name='category_blog'),
]
