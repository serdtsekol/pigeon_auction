import math

from django import template
from django.urls import reverse
from ..get_data import *

register = template.Library()


@register.simple_tag
def tag_user_orders(user_pk):
    return get_orders_user(user_pk)
