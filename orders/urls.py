from django.contrib import admin
from django.urls import path, include, re_path
from .views import *

app_name = 'orders'
urlpatterns = [
    path('order_created/<int:price>/<int:product_id>/', order_created, name='order_created'),
]
