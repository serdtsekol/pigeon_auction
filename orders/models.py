from django.contrib.auth.models import User
from django.db import models
from products.models import Product


# Create your models here.

class Order(models.Model):
    price_PLN = models.IntegerField()
    product_id = models.IntegerField()
    product = models.CharField(max_length=255)
    status = models.BooleanField(default=False)
    user_id = models.IntegerField()
    user_name = models.CharField(max_length=255, default="")
    user_email = models.CharField(max_length=255, default="")
    user_first_name = models.CharField(max_length=255, default="")
    user_last_name = models.CharField(max_length=255, default="")
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)
# class CategoryLanguage(models.Model):
#     language = models.CharField(_('lang'), max_length=15, choices=LANGUAGES)
#     link = models.CharField(null=False, max_length=100)
#     name = models.CharField(null=False, max_length=100)
#     title = models.CharField(null=False, max_length=100)
#     description = RichTextField(null=True, blank=True, default="")
#     category_fk = models.ForeignKey(Category, on_delete=models.CASCADE)
