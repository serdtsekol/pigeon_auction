from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Order
from products.models import Product
from mainapp.get_data import get_currency_pln


def order_created(request, price, product_id):
    user = User.objects.get(pk=request.user.pk)
    product = Product.objects.get(pk=product_id)
    order = Order.objects.create(price_PLN=price, product_id=product.pk, product=product.__str__(), user_id=user.pk, user_name=user.username, user_email=user.email, user_first_name=user.first_name,
                                 user_last_name=user.last_name)
    order.save()
    request.session['order_id'] = order.pk
    return redirect(reverse('process'))
