import datetime

from django.utils.timezone import now

from .models import Order
from django.utils.translation import get_language


def get_orders_user(user_pk):
    """ Всі замовлення юзера """
    result = []
    orders = Order.objects.filter(user_id=user_pk)
    for order in orders:
        result.append({
            'object': order,
            'data': {}
        })
    return result
