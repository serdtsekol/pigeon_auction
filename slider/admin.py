from django.contrib import admin
from tabbed_admin import TabbedModelAdmin

from .models import *


# Register your models here.

class SlideInline(admin.TabularInline):
    model = Slide
    extra = 1


@admin.register(Slider)
class SliderAdmin(TabbedModelAdmin):
    tab_Overview = (
        (None, {
            'fields': (
                'name', 'active',
            )
        }),
    )
    tab_Slide = (
        SlideInline,
    )
    tabs = [
        ('General', tab_Overview),
        ('Slides', tab_Slide),
    ]

    class Meta:
        model = Slider


class SlideYoutubeInline(admin.TabularInline):
    model = SlideYoutube
    extra = 1


@admin.register(SliderYoutube)
class SliderYoutubeAdmin(TabbedModelAdmin):
    tab_Overview = (
        (None, {
            'fields': (
                'name', 'active',
            )
        }),
    )
    tab_Slide = (
        SlideYoutubeInline,
    )
    tabs = [
        ('General', tab_Overview),
        ('Slides', tab_Slide),
    ]

    class Meta:
        model = SliderYoutube
