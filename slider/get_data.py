from .models import *


def get_slider_by_name(name):
    result = {}
    slider = Slider.objects.filter(name=name, active=True)
    if slider.first():
        result = {
            'object': slider.first(),
            'data': slider.first().get_fields(),
        }
    return result
