# Generated by Django 2.1.4 on 2019-02-06 23:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('slider', '0003_auto_20190207_0129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slideyoutube',
            name='video_html',
            field=models.TextField(default=''),
        ),
    ]
