import math

from django import template
from django.shortcuts import render
from django.template.loader import render_to_string
from django.urls import reverse
from ..models import SliderYoutube, SlideYoutube

register = template.Library()


@register.simple_tag
def tag_get_slider_youtube(slider_name):
    """ Всі ставки користувача """
    slids = SlideYoutube.objects.filter(slider_fk__name=slider_name)
    return render_to_string('slider/video_youtube.html', {'slids': slids})
