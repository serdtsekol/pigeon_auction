from django.db import models

from filebrowser.fields import FileBrowseField


class Slider(models.Model):
    name = models.CharField(null=False, max_length=100)
    active = models.BooleanField(default=True)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.name)

    def get_fields(self):
        result = []
        slids = Slide.objects.filter(slider_fk=self.pk)
        for slide in slids:
            result.append({
                'image': slide.image
            })
        return result


class Slide(models.Model):
    image = FileBrowseField("Image", max_length=200, directory="slider/", extensions=[".jpg", ".webp", ".png"], blank=True)
    slider_fk = models.ForeignKey(Slider, on_delete=models.CASCADE)


class SliderYoutube(models.Model):
    name = models.CharField(null=False, max_length=100)
    active = models.BooleanField(default=True)
    added_date = models.DateTimeField(auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __str__(self):
        return str(self.name)


class SlideYoutube(models.Model):
    video_html = models.TextField(null=False, default='')
    slider_fk = models.ForeignKey(SliderYoutube, on_delete=models.CASCADE)
