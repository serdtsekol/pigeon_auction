from datetime import datetime, timedelta
from django.utils import timezone
from django_cron import CronJobBase, Schedule
from mainapp.models import Profile
from pigeon import settings


class EmailUsercountCronJob(CronJobBase):
    """
    Send an email with the user count.
    """
    RUN_EVERY_MINS = 1  # 6 hours when not DEBUG

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'cron.EmailUsercountCronJob'

    def do(self):
        try:
            users = Profile.objects.all()
            print(users)
            for user in users:
                if user.last_seen:
                    now = timezone.now()
                    if now > user.last_seen + timedelta(seconds=settings.USER_ONLINE_TIMEOUT):
                        user.online = False
                    else:
                        user.online = True
                else:
                    user.online = False
                user.save()
        except Exception as ex:
            print(ex)
        print('asdddddddddddddddddddd')