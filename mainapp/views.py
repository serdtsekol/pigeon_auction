import json

from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect

from mainapp.models import Profile
from slider.get_data import *


# Create your views here.


def index(request):
    return render(request, 'mainapp/index.html', {'main_slider_top': get_slider_by_name('Reklamy')})


def contacts(request):
    return render(request, "mainapp/contacts.html")


def reglament(request):
    return render(request, "mainapp/reglament.html")


def logout_user(request):
    user = request.user
    logout(request)
    return redirect('/')
    # return HttpResponse("success")


def register_page(request):
    if request.user.is_authenticated:
        return redirect('/')
    return render(request, "mainapp/register.html", locals())


def login_page(request):
    if request.user.is_authenticated:
        return redirect('/')
    return render(request, "mainapp/login.html", locals())


def account_page(request):
    if not request.user.is_authenticated:
        return redirect('/')
    return render(request, "mainapp/account.html", {'profile': Profile.objects.get(user_fk_id=request.user.pk)})
