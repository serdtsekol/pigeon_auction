from django.contrib import admin
from django.urls import path, include
from .views import *
from .api import *

urlpatterns = [
    path('', index, name="index"),
    path('contacts/', contacts, name="contacts"),
    path('reglament/', reglament, name="reglament"),
    path("register/", register_page, name="register_page"),
    path("logout_user/", logout_user, name="logout_user"),
    path("login/", login_page, name="login_page"),
    path("account/", account_page, name="account_page"),
    path("api/login_json/", login_json, name="login_json"),
    path("api/register_json/", register_json, name="register_json"),
]
