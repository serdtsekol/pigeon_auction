import math

from django import template
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from blog.get_data import get_article_by_link, get_category_by_link as get_article_category_by_link
from mainapp.get_data import get_currency_pln
from products.get_data import get_product_by_link, get_category_by_link as get_product_category_by_link, get_bets_by_user_pk, get_bets_win_by_user_pk
from forum.get_data import get_forum_category_by_url, get_forum_article_by_url

register = template.Library()


@register.simple_tag
def tag_breadcrumbs(request):
    breadcrumbs = []
    url = request.path
    urls = url.split('/')
    urls = [var for var in urls if var]
    for link in urls:
        if link == 'blog':
            breadcrumbs.append({'title': _('Blog'), 'link': reverse('blog')})
            continue
        if link == 'blog_sorting':
            breadcrumbs.append({'title': _('Fancier index'), 'link': reverse('blog_sorting')})
            continue
        if link == 'contacts':
            breadcrumbs.append({'title': _('Contacts'), 'link': reverse('contacts')})
            continue
        if link == 'catalog':
            breadcrumbs.append({'title': _('Catalog'), 'link': reverse('catalog')})
            continue
        if link == 'archive':
            breadcrumbs.append({'title': _('Archive'), 'link': reverse('archive')})
            continue
        if link == 'soon':
            breadcrumbs.append({'title': _('Soon'), 'link': reverse('soon')})
            continue
        if link == 'forum':
            breadcrumbs.append({'title': _('Forum'), 'link': reverse('forum')})
            continue

        """Категорії форума"""
        try:
            category = get_forum_category_by_url(link)
            breadcrumbs.append({'title': category.language.name, 'link': category.language.link})
            continue
        except Exception as error:
            pass
        """Статті форума"""
        try:
            article = get_forum_article_by_url(link)
            breadcrumbs.append({'title': article.name, 'link': article.link})
            continue
        except Exception as error:
            pass

        """Категорії товарів"""
        try:
            category = get_product_category_by_link(link)
            breadcrumbs.append({'title': category['data']['name'], 'link': category['data']['link']})
            continue
        except Exception as error:
            pass

        """Товари"""
        try:
            product = get_product_by_link(link)
            breadcrumbs.append({'title': product['data']['name'], 'link': product['data']['link']})
            continue
        except Exception as error:
            pass

        """Категорії статей"""
        try:
            category = get_article_category_by_link(link)
            breadcrumbs.append({'title': category['data']['name'], 'link': category['data']['link']})
            continue
        except Exception as error:
            pass

        """Статті"""
        try:
            article = get_article_by_link(link)
            breadcrumbs.append({'title': article['data']['name'], 'link': article['data']['link']})
            continue
        except Exception as error:
            pass

    return breadcrumbs


@register.simple_tag
def tag_user_bets(user_pk):
    """ Всі ставки користувача """
    return get_bets_by_user_pk(user_pk)


@register.simple_tag
def tag_user_bets_win(user_pk):
    """ Всі виграні ставки користувача """
    return get_bets_win_by_user_pk(user_pk)


@register.simple_tag
def tag_get_currency_pln():
    return get_currency_pln()


@register.filter
def filter_convert_eur_to_pln(price):
    pln_coficient = get_currency_pln()
    price = price / pln_coficient['data']['price']
    return math.ceil(price)
