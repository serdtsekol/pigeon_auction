from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import *


# Register your models here.

@admin.register(CurrencySettings)
class CurrencySettingsAdmin(admin.ModelAdmin):
    list_display = [field.name for field in CurrencySettings._meta.fields if field.name != "id"]


# @admin.register(Profile)
# class ProfileAdmin(admin.ModelAdmin):
#     list_display = [field.name for field in Profile._meta.fields if field.name != "id"]

# Register your models here.
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = "Info"

class ProfileAdmin(UserAdmin):
    inlines = (ProfileInline,)

# Определяем новый класс настроек для модели User



# Перерегистрируем модель User
admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)