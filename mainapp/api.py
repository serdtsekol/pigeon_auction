import errno
import json
import os

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponse
from django.shortcuts import redirect

from filebrowser.base import FileObject
from mainapp.models import Profile
from pigeon.settings import MEDIA_ROOT

def login_json(request):
    if request.method == 'GET':
        return redirect('/')
    data = request.POST
    user_login = data.get('auth_name')
    user_password = data.get('auth_password')
    user = authenticate(username=user_login, password=user_password)
    if user is not None:
        if request.user.is_authenticated:
            return redirect('/')
        else:
            login(request, user)
            if request.user.is_authenticated:
                result_request = {'success': True}
                return JsonResponse(result_request, safe=False)
            else:
                result_request = {'success': False}
                return JsonResponse(result_request, safe=False)
    else:
        result_request = {'success': False}
        return JsonResponse(result_request, safe=False)


def register_json(request):
    data = request.POST
    user_name = data.get('register_name')
    user_first_name = data.get('register_first_name')
    user_last_name = data.get('register_last_name')
    user_phone = data.get('register_phone')
    user_email = data.get('register_email')
    user_password = data.get('register_password')
    file_name = f'{MEDIA_ROOT}/account/{user_name}/{request.FILES["register_image"].name}'
    file_name_model = f'account/{user_name}/{request.FILES["register_image"].name}'
    save_file(request.FILES['register_image'].read(), file_name)
    user, created = User.objects.get_or_create(username=user_name, email=user_email, first_name=user_first_name, last_name=user_last_name)

    if created:
        user.set_password(user_password)  # This line will hash the password
        user.save()  # DO NOT FORGET THIS LINE
        profile = Profile.objects.create(phone=user_phone, user_fk=user, image=file_name_model)
        profile.save()

        result_request = {'success': True}
        return JsonResponse(result_request, safe=False)
    else:
        result_request = {'success': False, 'message': 'User Exists'}
        return JsonResponse(result_request, safe=False)

def save_file(file_byte, name):
    if not os.path.exists(os.path.dirname(name)):
        try:
            os.makedirs(os.path.dirname(name))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    try:
        with open(name, "wb") as file:
            file.write(file_byte)
    except Exception as ex:
        print(ex)