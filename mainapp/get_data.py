import datetime

from django.utils.timezone import now

from .models import *
from django.utils.translation import get_language


def get_currency_pln():
    """ Получає валюту PLN data {name,price} """
    PLN = CurrencySettings.objects.first()
    result = {}
    if PLN:
        result = {
            'object': PLN,
            'data': {'name': PLN.name, 'price': PLN.price}
        }
    return result
