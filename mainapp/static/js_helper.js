function getTimeRemaining(endtime) {
    let t = Date.parse(endtime) - Date.parse(new Date());


    let seconds = Math.floor((t / 1000) % 60);
    let minutes = Math.floor((t / 1000 / 60) % 60);
    let hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    let days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    let clock = document.getElementById(id);
    let daysSpan = clock.querySelector('.days');
    let hoursSpan = clock.querySelector('.hours');
    let minutesSpan = clock.querySelector('.minutes');
    let secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        let t = getTimeRemaining(endtime);
        if (t.total < 0) {
            daysSpan.innerHTML = 0;
            hoursSpan.innerHTML = 0;
            minutesSpan.innerHTML = 0;
            secondsSpan.innerHTML = 0;
            return;
        }
        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    let timeinterval = setInterval(updateClock, 1000);
}

let form_login = $('[id^=auth_form-]');
form_login.on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        url: form_login.attr("action"),
        type: "POST",
        data: {'auth_name': $(e.target).find('[id^=auth_name-]').val(), 'auth_password':  $(e.target).find('[id^=auth_password-]').val(), 'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val()},
        success: function (data) {
            if (data['success'] === true) {
                // alert('Успішно заголінився');
                document.location.href = "/account/";
            } else if (data['success'] === false) {
                alert("Login or password is incorrect");
            }
        }
    })
});

let form_register = $('[id^=register_form-]');
form_register.on('submit', function (e) {
    e.preventDefault();
    let register_name = $('[id^=register_name-]').val();
    let register_email = $('[id^=register_email-]').val();
    let register_password = $('[id^=register_password-]').val();
    let register_password_confirm = $('[id^=register_password-]').val();
    let register_first_name = $('[id^=register_first_name-]').val();
    let register_last_name = $('[id^=register_last_name-]').val();
    let register_phone = $('[id^=register_phone-]').val();
    let register_image = $('[id^=register_image-]');

    if (register_password !== register_password_confirm) {
        alert('Passwords do not match');
    }

    if (register_password === "" || register_password_confirm === "") {
        alert('Set Password');
    }
    if (register_phone === "") {
        alert('Phone Required')
    }
    if (register_image === "") {
        alert('Image Required')
    }
    var fd = new FormData;
    fd.append('register_name', register_name);
    fd.append('register_email', register_email);
    fd.append('register_password', register_password);
    fd.append('register_first_name', register_first_name);
    fd.append('register_last_name', register_last_name);
    fd.append('register_phone', register_phone);
    fd.append('register_image', register_image[0].files[0]);
    fd.append('csrfmiddlewaretoken', $('[name="csrfmiddlewaretoken"]').val());

    $.ajax({
        url: form_register.attr("action"),
        type: "POST",
        processData: false,
        contentType: false,
        data: fd,
        success: function (data) {
            if (data['success'] === true) {
                alert('Registration was successful');
                document.location.href = '/login/';
            } else if (data['success'] === false) {
                alert("Check the entered data");
            }
        }
    })
});

let bet_add_button = $('[id^=bet_add_button]');
bet_add_button.on('click', (e) => {

    let bet_input = $('#bets_input');
    $.ajax({
        url: bet_add_button.data('href'),
        type: "POST",
        data: {'price': bet_input.val(), 'product-pk': bet_input.data('product-pk'), 'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val()},
        success: function (data) {
            if (data['success'] === true) {
                alert('The bid was made successfully');
                document.location.reload();
            } else if (data['success'] === false) {
                alert(data['message']);
            }
        }
    });
});

/*REGION FORUM*/

try {
    CKEDITOR.replace('create-comment');
} catch (e) {

}

try {
    CKEDITOR.replace('article-description');
} catch (e) {

}


$("body").on('click', '#button-create-comment', (e) => {
    console.log(e.target.dataset.article_id);
    $.ajax({
        url: e.target.dataset.url,
        type: "POST",
        data: {
            'comment': CKEDITOR.instances["create-comment"].getData(),
            'article_id': e.target.dataset.article_id,
            'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val()
        },
        success: function (data) {
            if (data['success'] === true) {
                document.location.reload();
            } else if (data['success'] === false) {
                alert(data['error']);
            }
        }
    })
});

$("body").on('click', '#forum-article-add', (e) => {
    $.ajax({
        url: e.target.dataset.url,
        type: "POST",
        data: {
            'description': CKEDITOR.instances["article-description"].getData(),
            'category_id': $('#article-categories option:selected').val(),
            'name': $('#article-name').val(),
            'csrfmiddlewaretoken': $('[name="csrfmiddlewaretoken"]').val()
        },
        success: function (data) {
            if (data['success'] === true) {
                document.location.reload();
            } else if (data['success'] === false) {
                alert(data['error']);
            }
        }
    })
});

/*END REGION FORUM*/

try {
    CKEDITOR.replace('lot_description_en');
    CKEDITOR.replace('lot_description_pl');
    CKEDITOR.replace('lot_description_de');
} catch (e) {

}

$("body").on('click', '#add_auction_lot', (e) => {
    $(".add-lot-modal").show();
    $("body").css('overflow','hidden')
});
$('body').on('click', '.close-lot-modal', (e) => {
    $(".add-lot-modal").hide();
    $("body").css('overflow','unset')
});

$('body').on('submit', '#add_lot', (e) => {
    e.preventDefault();
    const languages = JSON.parse($("#add_lot").data('languages').replace(/\(/g, '{').replace(/\)/g, '}').replace(/\'/g, '"').replace(/\",/g, '":'));
    let array_attributes_of_product = [];
    let array_images = [];
    $('.product_attributes').each((key, element) => {
        array_attributes_of_product.push({'attribute_fk_id': $(element).data('attr_id'), 'attribute_value_fk_id': $(element).data('attr_val_id')})
    });

    $('#gallery > div > img').each((key, element) => {
        array_images.push({'image_content': $(element).attr('src'), 'name': $(element).data('name')});
    });

    let result = {
        'first_price': e.target.elements['firs_price'].value,
        'time_start': e.target.elements['time_start'].value,
        'time_end': e.target.elements['time_end'].value,
        'attributes_of_values': array_attributes_of_product,
        'array_images': array_images,
    };
    languages.map((value, counter) => {
        const language = Object.keys(value)[0];
        result['languages'] = {...result['languages'], ...{[language]: {'name': e.target.elements['lot_name_' + language].value, 'description': e.target.elements['lot_name_' + language].value}}};
        // result[Object.keys(value)[0]] = {'name': e.target.elements['lot_name_' + Object.keys(value)[0]].value, 'description': e.target.elements['lot_name_' + Object.keys(value)[0]].value};
    });
    console.log(result);
    $.ajax({
        url: e.target.action,
        type: "POST",
        data: {'data': JSON.stringify(result)},
        success: function (data) {
            if (data['success'] === true) {
                alert('Lot Added');
                $(".add-lot-modal").hide();
                document.location.reload()
            } else if (data['success'] === false) {
                alert(data['error']);
            }
        }
    })
});

/*ЗМІНА АТРИБУТА В СЕЛЕКТІ ДОДАЄ ЗНАЧЕННЯ АТРИБУТІВ В ДРУГОМУ СЕЛЕКТІ*/
$('body').on('change', '#select_attribute', (e) => {
    let selected_attribute = $(e.target).find('option:selected').val();
    $.ajax({
        url: $(e.target).data('url'),
        type: "POST",
        data: {
            'id': selected_attribute
        },
        success: function (data) {
            $('#add_attribute').hide();
            $('label[for="select_attribute_value"]').show();
            $('#select_attribute_value').show().html(data);
        }
    })
});
$('body').on('change', '#select_attribute_value', (e) => {
    if ($(e.target).find('option:selected').index() !== 0) {
        $('#add_attribute').show();
    } else {
        $('#add_attribute').hide();
    }
});

$('body').on('click', '#add_attribute', (e) => {
    let attr_id = $('#select_attribute option:selected').val();
    let attr_text = $('#select_attribute option:selected').text();
    let attr_val_id = $('#select_attribute_value option:selected').val();
    let attr_val_text = $('#select_attribute_value option:selected').text();
    let can_add = true;
    $('.product_attributes').each((key, element) => {
        if ($(element).data('attr_id') == attr_id) {
            can_add = false;
        }
    });
    if (can_add !== false) {
        $('#list_added_attributes').html($('#list_added_attributes').html() + `<div class="product_attributes" data-attr_id="${attr_id}" data-attr_val_id="${attr_val_id}">${attr_text}: ${attr_val_text} <i class="fa fa-times delete-attribute"></i></div>`)
        $('label[for="select_attribute_value"]').hide();
        $('#add_attribute').hide();
        $('#select_attribute_value').hide();
        $('#select_attribute_value option:first-child').prop("selected", true);
        $('#select_attribute option:first-child').prop("selected", true);
    }

});
$('body').on('click', '.delete-attribute', (e) => {
    $(e.target).parent().remove();
});