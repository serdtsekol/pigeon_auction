from django.contrib.auth.models import User
from django.db import models
from django.core.cache import cache
from django.conf import settings
from datetime import datetime, timedelta
from django.utils import timezone
# Create your models here.
from ckeditor_uploader.fields import RichTextUploadingField
from filebrowser.base import FileObject
from filebrowser.fields import FileBrowseField


class CurrencySettings(models.Model):
    name = models.CharField(null=False, max_length=100)
    price = models.DecimalField(null=False, max_digits=9, decimal_places=5)

    def __str__(self):
        return self.name


class Profile(models.Model):
    phone = models.CharField(max_length=30, default="")
    user_fk = models.OneToOneField(User, on_delete=models.CASCADE, default=None, blank=True, null=True)
    online = models.BooleanField(default=False)
    image = models.ImageField(upload_to='account', default='no-image.png')
    description = RichTextUploadingField(null=True, blank=True, default="")
    last_seen = models.DateTimeField(auto_now_add=False, auto_now=False, default=timezone.now)

    def __str__(self):
        return self.user_fk.username

    def method_last_seen(self):
        return cache.get('seen_%s' % self.user_fk.username)

    def method_online(self):
        if self.method_last_seen():
            now = datetime.now()
            if now > self.method_last_seen() + timedelta(seconds=settings.USER_ONLINE_TIMEOUT):
                return False
            else:
                return True
        else:
            return False
