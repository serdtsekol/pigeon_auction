import datetime

from django.core.paginator import Paginator
from django.http import Http404
from django.shortcuts import render, redirect

from forum.get_data import get_forum_category_by_id, get_forum_article_by_url, get_all_categories, get_forum_category_by_url, get_all_forum_articles_in_forum_category, get_all_forum_category_children
from mainapp.models import Profile
from pigeon import settings
FORUM_ITEMS_PER_PAGE = 6

def forum(request):
    users = Profile.objects.all()
    categories = get_all_categories()
    return render(request, 'forum/forum.html', {'users': users, 'categories': categories})

def dialogs(request):
    profile = request.user.profile
    print(profile.sender.all().values('forum_profile_sender_fk_id'))
    print(profile.recipient.all().values('forum_profile_recipient_fk_id'))
    return render(request, 'forum/dialogs.html', {})


def forum_article_or_category(request, forum_category_link):
    forum_category_slugs = [str(x) for x in forum_category_link.split('/')]
    try:
        forum_article = get_forum_article_by_url(forum_category_slugs[-1])
        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        # if forum_article['redirect']:
        #     return redirect(forum_article['object'].get_absolute_url())
        # if forum_article['object'].get_absolute_url() != request.path:
        #     return redirect(forum_article['object'].get_absolute_url())
        _forum_category = get_forum_category_by_id(forum_article.forum_category_fk.id) if forum_article.forum_category_fk else None
        result_request = {
            "forum_article": forum_article,
            "forum_category": _forum_category,
            "categories": get_all_categories(),
        }
        return render(request, 'forum/forum_article.html', result_request)
    except Exception as ex:
        print("not forum_article", ex)

    try:
        forum_category = get_forum_category_by_url(forum_category_slugs[-1])

        """ Якщо ссилка в іншій мові відрізняється то перейти на неї """
        if forum_category.redirect:
            return redirect(forum_category.get_absolute_url())
        if forum_category.get_absolute_url() != request.path:
            return redirect(forum_category.get_absolute_url())

        forum_articles = get_all_forum_articles_in_forum_category(forum_category.pk)
        paginator = Paginator(forum_articles, FORUM_ITEMS_PER_PAGE)
        if request.method == "GET":
            page = paginator.page(request.GET.get('page', 1))
            forum_articles = page
        result_request = {
            "forum_category": forum_category,
            "categories": get_all_categories(),
            "categories_children": get_all_forum_category_children(forum_category.pk),
            "forum_articles": forum_articles,
            "users": Profile.objects.all(),

        }
        return render(request, 'forum/forum_category.html', result_request)
    except Exception as ex:
        print("not forum_category", ex)

    raise Http404("NotArticleAndNOtCategory")
