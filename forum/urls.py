from django.contrib import admin
from django.urls import path, include, re_path

from .views import forum, forum_article_or_category, dialogs
from .api import create_comment, create_forum_article
urlpatterns = [
    path('forum/', forum, name="forum"),
    path('forum/dialogs/', dialogs, name="dialogs"),
    re_path(r'^forum/(?P<forum_category_link>.+)/$', forum_article_or_category, name="forum_article_or_category"),
    path('api/create_comment/', create_comment, name="create_comment"),
    path('api/create_forum_article/', create_forum_article, name="create_forum_article"),
]
