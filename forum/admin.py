from django.contrib import admin
from django.forms import TextInput, Textarea
from django.db import models
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from pigeon import settings
from suit.admin import SortableModelAdmin
from mptt.admin import MPTTModelAdmin
from tabbed_admin import TabbedModelAdmin
from .models import ForumArticle, ForumCategory, ForumCategoryLanguage, Comment, MessageToUser


class ForumCategoryLanguageInline(admin.TabularInline):
    model = ForumCategoryLanguage
    max_num = len(settings.LANGUAGES)
    extra = len(settings.LANGUAGES)
    prepopulated_fields = {'link': ('title',)}
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '20'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 40})},
    }
    verbose_name = _('admin__language_field')
    verbose_name_plural = _('admin__language_field')


@admin.register(ForumArticle)
class ForumArticleAdmin(TabbedModelAdmin):
    # change_form_template = 'admin_custom/forum_article.html'
    list_display = ['name_admin'] + [field.name for field in ForumArticle._meta.fields]
    list_editable = [field.name for field in ForumArticle._meta.fields if field.name != "id" and field.name != "added_date" and field.name != "update_date"]
    # search_fields = ('',)
    # inlines = (ForumArticleLanguageInline,)
    tab_Overview = (
        (None, {
            'fields': ('image', 'forum_category_fk', 'show_in_categories', 'active', 'link', 'name', 'seo_title', 'seo_description', 'description', 'forum_profile_fk')
        }),
    )

    tabs = [
        (_('admin__general'), tab_Overview),
    ]

    # inlines = (ForumArticleLanguageInline,)

    class Meta:
        model = ForumArticle

    def name_admin(self, instance):
        return format_html(
            '<div>{}</div>',
            instance.__str__(),
        )

    def save_model(self, request, obj, form, change):
        try:
            print('save save')
            if not obj.show_in_categories.filter(id=obj.forum_category_fk.id).exists():
                obj.show_in_categories.add(obj.forum_category_fk.id)
                form.cleaned_data['show_in_categories'] = obj.show_in_categories.all()
        except Exception as ex:
            print(ex)
        super(ForumArticleAdmin, self).save_model(request, obj, form, change)


@admin.register(ForumCategory)
class ForumCategoryAdmin(MPTTModelAdmin):
    mptt_level_indent = 20
    list_display = ['name_admin'] + [field.name for field in ForumCategory._meta.fields]
    list_editable = ('parent',)
    list_display_links = ('id', 'name_admin')
    sortable = 'order'
    inlines = (ForumCategoryLanguageInline,)

    def name_admin(self, instance):
        return format_html(
            '<div style="text-indent:{}px">{}</div>',
            instance._mpttfield('level') * self.mptt_level_indent,
            instance.__str__(),  # Or whatever you want to put here
        )


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    pass


@admin.register(MessageToUser)
class MessageToUserAdmin(admin.ModelAdmin):
    pass
