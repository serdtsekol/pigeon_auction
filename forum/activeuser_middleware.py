import datetime
from django.core.cache import cache
from django.conf import settings

class CorsMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response["Access-Control-Allow-Origin"] = "*"
        response["Access-Control-Allow-Headers"] = "*"
        response["X-Frame-Options"] = "allow-from golden-web.digital"
        current_user = request.user
        if request.user.is_authenticated:
            now = datetime.datetime.now()
            cache.set('seen_%s' % (current_user.username), now, settings.USER_LASTSEEN_TIMEOUT)
            current_user.profile.online = current_user.profile.method_online()
            current_user.profile.last_seen = current_user.profile.method_last_seen()
            current_user.profile.save()
        return response

#
# class ActiveUserMiddleware:
#
#     def process_request(self, request):
#         current_user = request.user
#         if request.user.is_authenticated():
#             now = datetime.datetime.now()
#             cache.set('seen_%s' % (current_user.username), now, settings.USER_LASTSEEN_TIMEOUT)
