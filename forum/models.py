import hashlib
from datetime import datetime, timedelta
from django.utils.translation import get_language

from ckeditor.fields import RichTextField
from django.core.exceptions import ValidationError
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from filebrowser.fields import FileBrowseField
from mainapp.models import Profile
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from django.urls import reverse
from pigeon import settings
from slugify import slugify
from pigeon.settings import LANGUAGES


class ForumCategory(MPTTModel):
    image = FileBrowseField(verbose_name=_('admin__image'), max_length=200, directory="forum/", extensions=[".jpg", ".webp", ".png", ".jpeg"], blank=True)
    order = models.PositiveIntegerField(verbose_name=_('admin__order'), default=0)
    active = models.BooleanField(verbose_name=_('admin__active'), default=True)
    parent = TreeForeignKey('self', verbose_name=_('admin__parent'), null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    added_date = models.DateTimeField(verbose_name=_('admin__added_date'), auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(verbose_name=_('admin__update_date'), auto_now_add=False, auto_now=True)

    class MPTTMeta:
        order_insertion_by = ['order']

    class Meta:
        verbose_name = _('admin__forum_category')
        verbose_name_plural = _('admin__categories')

    def save(self, *args, **kwargs):
        super(ForumCategory, self).save(*args, **kwargs)
        ForumCategory.objects.rebuild()

    def __str__(self):
        try:
            return str(self.forumcategorylanguage_set.filter(language=get_language()).first().name)
        except Exception as ex:
            return str(self.id)

    def count_articles(self):
        return ForumArticle.objects.filter(forum_category_fk_id=self.id).count()

    def date_last_article(self):
        try:
            return ForumArticle.objects.first().added_date
        except Exception as ex:
            return ""

    def get_all_children_and_your_father(self):
        children = [self]
        try:
            child_list = self.children.all()
        except AttributeError:
            return children
        for child in child_list:
            children.extend(child.get_all_children_and_your_father())
        return children

    def get_all_children(self):
        all_children = self.get_all_children_and_your_father()
        all_children.remove(all_children[0])
        return all_children

    def get_all_parents(self):
        parents = [self]
        if self.parent is not None:
            parent = self.parent
            parents.extend(parent.get_all_parents())
        return parents

    def get_absolute_url(self):
        link = ""
        for parent in reversed(self.get_all_parents()):
            forum_category_lng = ForumCategoryLanguage.objects.get(language=get_language(), forum_category_fk=parent.pk)
            link += forum_category_lng.link + "/"
        link = link[:-1]
        return reverse('forum_article_or_category', args=[str(link)])

    def clean(self):
        if self.parent in self.get_all_children():
            raise ValidationError("Error ForumCategory")


class ForumCategoryLanguage(models.Model):
    language = models.CharField(_('lang'), max_length=15, choices=settings.LANGUAGES)
    link = models.CharField(verbose_name=_('admin__link'), null=True, blank=True, default='', max_length=100)
    name = models.CharField(verbose_name=_('admin__name'), null=False, max_length=100)
    title = models.CharField(verbose_name=_('admin__title'), null=False, max_length=100)
    seo_description = models.TextField(verbose_name=_('admin__seo_description'), null=False)
    description = RichTextUploadingField(verbose_name=_('admin__description'), null=True, blank=True, default="")
    forum_category_fk = models.ForeignKey(ForumCategory, verbose_name=_('admin__forum_category'), on_delete=models.CASCADE)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.link is None or self.link == "":
            self.link = slugify(self.name, to_lower=True)
        super(ForumCategoryLanguage, self).save()


class ForumArticle(models.Model):
    image = FileBrowseField(verbose_name=_('admin__forum_article_image'), max_length=255, directory="blog/", extensions=[".jpg", ".webp", ".png", ".jpeg"], blank=True)
    link = models.CharField(verbose_name=_('admin__link'), blank=True, null=True, default='', max_length=255)
    name = models.CharField(verbose_name=_('admin__name'), null=False, max_length=255)
    seo_title = models.CharField(verbose_name=_('admin__seo_title'), blank=True, default='', max_length=255)
    seo_description = models.TextField(verbose_name=_('admin__seo_description'), null=True, blank=True, default="")
    description = RichTextUploadingField(verbose_name=_('admin__content'), null=True, blank=True, default="")
    forum_profile_fk = models.ForeignKey(Profile, verbose_name=_('admin__profile'), on_delete=models.SET_NULL, blank=True, null=True)
    forum_category_fk = models.ForeignKey(ForumCategory, verbose_name=_('admin__forum_article_forum_category'), on_delete=models.CASCADE, related_name='main_forum_category', blank=True, null=True)
    show_in_categories = models.ManyToManyField(ForumCategory, verbose_name=_('admin__forum_article_show_in_categories'), related_name='show_in_categories', blank=True)
    active = models.BooleanField(verbose_name=_('admin__forum_article_active'), default=True)
    added_date = models.DateTimeField(verbose_name=_('admin__forum_article_added_date'), auto_now_add=True, auto_now=False)
    update_date = models.DateTimeField(verbose_name=_('admin__forum_article_update_date'), auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = _('admin__forum_article')
        verbose_name_plural = _('admin__forum_articles')

    def count_answers(self):
        return Comment.objects.filter(forum_article_fk_id=self.id).count()

    def date_last_answer(self):
        try:
            return Comment.objects.first().added_date
        except Exception as ex:
            return self.added_date

    def __str__(self):
        try:
            return str(self.name)
        except Exception as ex:
            return str(self.id)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(ForumArticle, self).save()
        try:
            if self.seo_description is None or self.seo_description == "":
                self.seo_description = self.name
            if self.seo_title is None or self.seo_title == "":
                self.seo_title = self.name
            if self.link is None or self.link == "":
                self.link = slugify(self.name, to_lower=True)
            if not self.show_in_categories.filter(id=self.forum_category_fk.id).exists():
                self.show_in_categories.add(self.forum_category_fk.id)

        except Exception as ex:
            print(ex)
        super(ForumArticle, self).save()

    def get_absolute_url(self):
        if self.forum_category_fk:
            return str(self.forum_category_fk.get_absolute_url() + str(self.link) + "/")
        else:
            return str(reverse('forum_article', args=[str(self.link)]))


class Comment(models.Model):
    content = RichTextUploadingField(verbose_name=_('admin__content'), null=True, blank=True, default="")
    parent = models.ForeignKey('self', verbose_name=_('admin__parent'), null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    forum_profile_fk = models.ForeignKey(Profile, verbose_name=_('admin__profile'), on_delete=models.CASCADE, blank=True, null=True)
    forum_article_fk = models.ForeignKey(ForumArticle, verbose_name=_('admin__article'), on_delete=models.SET_NULL, blank=True, null=True)


class MessageToUser(models.Model):
    content = RichTextUploadingField(verbose_name=_('admin__content'), null=True, blank=True, default="")
    forum_profile_sender_fk = models.ForeignKey(Profile, related_name='sender', verbose_name=_('profile_sender'), on_delete=models.CASCADE, blank=True, null=True)
    forum_profile_recipient_fk = models.ForeignKey(Profile, related_name='recipient', verbose_name=_('profile_recipient'), on_delete=models.CASCADE, blank=True, null=True)
    added_date = models.DateTimeField(verbose_name=_('admin__forum_article_added_date'), auto_now_add=True, auto_now=False)
