from .models import ForumCategory, ForumCategoryLanguage, ForumArticle
from django.utils.translation import get_language


def get_all_categories(is_parents=True):
    """ Віддає всі включенні категорії (is_parents=True Тільки батьків) """
    if is_parents:
        categories = ForumCategory.objects.filter(active=True, parent=None)
    else:
        categories = ForumCategory.objects.filter(active=True)

    for forum_category in categories:
        forum_category.language = forum_category.forumcategorylanguage_set.filter(language=get_language()).first()
    return categories


def get_all_forum_category_children(id):
    """ Віддає всіх дітей категорії """
    forum_category = ForumCategory.objects.get(id=id, forumcategorylanguage__language=get_language())
    categories = forum_category.get_all_children()
    for forum_category in categories:
        forum_category.language = forum_category.forumcategorylanguage_set.filter(language=get_language()).first()
    return categories


def get_forum_category_by_url(link):
    forum_category = None
    try:
        forum_category = ForumCategory.objects.get(forumcategorylanguage__link=link, forumcategorylanguage__language=get_language())
        forum_category.language = forum_category.forumcategorylanguage_set.filter(language=get_language()).first()
        forum_category.redirect = False
    except Exception as ex:
        categories = ForumCategory.objects.filter(forumcategorylanguage__link=link)
        for cat in categories:
            cat.language = cat.forumcategorylanguage_set.filter(language=get_language()).first()
            if cat.language.language == get_language():
                forum_category = cat
        forum_category.redirect = True
    return forum_category


def get_forum_category_by_id(_id, _object=None):
    if not _object:
        forum_category = ForumCategory.objects.get(id=_id)
    else:
        forum_category = _object
    forum_category.language = forum_category.forumcategorylanguage_set.filter(language=get_language()).first()
    forum_category.redirect = False
    return forum_category


def get_last_forum_articles(limit=100):
    """ Віддає всі включенні останні статті """
    forum_articles = ForumArticle.objects.filter(active=True).order_by('-added_date')[:limit]
    return forum_articles


def get_all_forum_articles():
    """ Віддає всі включенні статті """
    forum_articles = ForumArticle.objects.filter(active=True).order_by('-added_date')
    return forum_articles


def get_all_forum_articles_in_forum_category(forum_category):
    """ Віддає всі статті в категорії """
    forum_articles = ForumArticle.objects.filter(active=True, show_in_categories=forum_category)
    return forum_articles


def get_forum_article_by_url(link):
    """ Віддає дані статті по ссилці"""
    forum_article = ForumArticle.objects.get(active=True, link=link)
    forum_article.redirect = False
    return forum_article


# def get_forum_article_by_id(_id):
#     """ Віддає дані статті по його id """
#     forum_article = ForumArticle.objects.get(active=True, id=_id, forumarticlelanguage__language=get_language())
#     forum_article.language = forum_article.forumarticlelanguage_set.filter(language=get_language()).first()
#     return forum_article
