from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _

from .models import ForumArticle, Comment


def create_comment(request):
    comment = request.POST.get('comment', None)
    article_id = request.POST.get('article_id', None)
    error = None
    responce = {}
    if comment is None or len(str(comment)) <= 10:
        responce.update({'success': False})
        responce.update({'error': _('comment_need_more_symbols')})
    else:
        responce.update({'success': True})
        responce.update({'comment': comment})
        Comment.objects.create(forum_profile_fk_id=request.user.profile.id, forum_article_fk_id=article_id, content=comment)
    return JsonResponse(responce, safe=False)


def create_forum_article(request):
    responce = {}
    error = None
    name = request.POST.get('name', None)
    category_id = request.POST.get('category_id', None)
    description = request.POST.get('description', None)
    print(request.user.profile.id)
    if name is None or len(str(name)) <= 10:
        responce.update({'success': False})
        responce.update({'error': _('name_need_more_symbols')})
    elif description is None or len(str(description)) <= 10:
        responce.update({'success': False})
        responce.update({'error': _('description_need_more_symbols')})
    else:
        responce.update({'success': True})
        ForumArticle.objects.create(forum_category_fk_id=category_id, name=name, description=description, forum_profile_fk_id=request.user.profile.id)
    return JsonResponse(responce, safe=False)
