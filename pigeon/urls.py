"""pigeon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.views.generic import TemplateView, RedirectView
from django.views.static import serve
from filebrowser.sites import site

from mainapp import context_processors

urlpatterns = [
                  path('admin/filebrowser/', site.urls),
                  path('grappelli/', include('grappelli.urls')),
                  path('admin/', admin.site.urls),
                  path('i18n/', include('django.conf.urls.i18n')),
                  path('lang/', context_processors.lang, name="lang"),
                  path('paypal/', include('paypal.standard.ipn.urls')),
                  path('orders/', include('orders.urls'), name='orders'),
                  path('payment/', include('payment.urls'), name='payment'),
                  re_path(r'^static/(?P<path>.*)$', serve,
                          {'document_root': settings.STATIC_ROOT}),
                  re_path(r'^media/(?P<path>.*)$', serve,
                          {'document_root': settings.MEDIA_ROOT}),
                  path('favicon.ico', RedirectView.as_view(url='/media/icons/faicon.ico'), name='favicon'),
                  path('robots.txt', TemplateView.as_view(template_name="robots.txt", content_type="text/plain"), name="robots_file"),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += i18n_patterns(
    path("", include("mainapp.urls")),
    path("", include("products.urls")),
    path("", include("blog.urls")),
    path("", include("slider.urls")),
    path("", include("send_email.urls")),
    path("", include("forum.urls")),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
    prefix_default_language=False,
)
